#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>

#include "graph.h"
#include "parameters.h"
#include "stop.h"
#include "svg.h"
#include "route.h"
#include "router.h"

class TransportManager {
public:
    TransportManager() : wait_time(0), velocity(0), router(nullptr) {};
    
    std::shared_ptr<Stop> AddStop(const std::string& name,
				  double lat,
				  double lon,
				  const std::map<std::string, int>& distances);
    std::shared_ptr<Stop> AddStop(const std::string& name,
				  const std::string& route_name);
    
    void AddRoute(const std::string& name,
		  const std::vector<std::string>& stops,
		  bool isCircular);

    std::ostream& DescribeRoute(const std::string& name,
				std::ostream& out) const;
    std::ostream& DescribeStop(const std::string& name,
			       std::ostream& out) const;
    Json::Node DescribeStopJSON(const std::string& name, int id) const;
    Json::Node DescribeRouteJSON(const std::string& name, int id) const;
    Json::Node DescribePathJSON(const std::string& from,
				const std::string& to,
				int id) const;
    Json::Node DescribePathStopJSON(const std::string& stop_name) const;
    Json::Node DescribePathBusJSON(const std::string& bus, int span, double time) const;
    Json::Node CreateMap(int id) const;
    
    void SetWaitTime(int wt);
    void SetVelocity(double v);
    void SetRenderParameters(const RenderParameters& p);
    int GetWaitTime() const;
    double GetVelocity() const;
    void Print() const;

    struct EdgeInfo {
	std::size_t from;
	std::size_t to;
	std::string name;
	double time;
	int span;
    };
private:
    Svg::Point GetStopCoords(std::shared_ptr<Stop> stop) const;
    void UpdateRenderParameters() const;
    void PlotRoutes(Svg::Document& map) const;
    void PlotStops(Svg::Document& map) const;
    void PlotStopNames(Svg::Document& map) const;
    void PlotRouteNames(Svg::Document& map) const;
    void PlotSingleRouteName(Svg::Document& map,
			     std::shared_ptr<Stop> stop,
			     std::string name,
			     int idx) const;
    
    void CreateEdgesForRoute(
	std::shared_ptr<Graph::DirectedWeightedGraph<double>> graph,
	std::shared_ptr<Route> route) const;
    void CreateEdgesForLinearRoute(
	std::shared_ptr<Graph::DirectedWeightedGraph<double>> graph,
	std::shared_ptr<Route> route) const;
    void CreateEdgesForCircularRoute(
	std::shared_ptr<Graph::DirectedWeightedGraph<double>> graph,
	std::shared_ptr<Route> route) const;
    
    std::map<std::string, std::shared_ptr<Stop>> stops;
    std::map<std::string, std::shared_ptr<Route>> routes;
    std::unordered_map<std::size_t, std::shared_ptr<Stop>> stops_id;
    
    int wait_time;
    double velocity;
    mutable RenderParameters render;
    
    mutable std::shared_ptr<Graph::Router<double>> router;
    mutable std::shared_ptr<Graph::DirectedWeightedGraph<double>> graph;
    mutable std::unordered_map<std::size_t, EdgeInfo> edges;
};
