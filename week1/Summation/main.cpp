#include <iostream>
#include <limits>

using namespace std;

int main() {
    int64_t a, b;
    cin >> a >> b;
    if (a >= 0) {
	if (b > numeric_limits<int64_t>::max() - a) {
	    cout << "Overflow!" << endl;
	    return 0;
	}
    }
    else {
	if (b < numeric_limits<int64_t>::min() - a) {
	    cout << "Overflow!" << endl;
	    return 0;
	}	
    }
    cout << a + b << endl;
    return 0;
}
