#include <string>
#include <utility>

// Исключение этого типа должно генерироваться при обращении к "пустому" Optional в функции Value
class BadOptionalAccess {
public:    
    BadOptionalAccess(const char* reason) : reason_(reason) {};
    BadOptionalAccess(const std::string& reason) : reason_(reason) {};
private:
    std::string reason_;
};

template <typename T>
class Optional {
private:
  // alignas нужен для правильного выравнивания блока памяти
  alignas(T) unsigned char data[sizeof(T)];
  bool defined = false;

public:
  Optional() = default;
  
  Optional(const T& elem) {
      new(data) T(elem);
      defined = true;
  };
  
  Optional(T&& elem) {
      new(data) T(std::move(elem));
      defined = true;
  };
  
  Optional(const Optional& other) {
      if (other.defined) {
	  new(data) T(other.Value());
	  defined = true;
      }
  };
  
  Optional(Optional&& other) {
      if (other.defined) {
	  new(data) T(std::move(other.Value()));
	  defined = true;
	  //other.defined = false;
      }
  };

  Optional& operator=(const T& elem) {
      if (defined) {
	  *reinterpret_cast<T*>(data) = elem;
      }
      else {
	  new(data) T(elem);
	  defined = true;
      }
      return *this;
  };
  
  Optional& operator=(T&& elem) {
      if (defined) {
	  *reinterpret_cast<T*>(data) = std::move(elem);
      }
      else {
	  new(data) T(std::move(elem));
      }
      defined = true;
      return *this;
  };
  
  Optional& operator=(const Optional& other) {
      if (other.defined) {
	  if (defined) {
	      *reinterpret_cast<T*>(data) = other.Value();
	  }
	  else {
	      new(data) T(other.Value());
	      defined = true;
	  }
      }
      else {
	  Reset();
      }
      return *this;
  };
  
  Optional& operator=(Optional&& other) {
      if (other.defined) {
	  if (defined) {
	      *reinterpret_cast<T*>(data) = std::move(other.Value());
	  }
	  else {
	      new(data) T(std::move(other.Value()));
	  }
	  defined = true;
	  //other.defined = false;
      }
      else {
	  Reset();
      }
      return *this;
  };

  bool HasValue() const {
      return defined;
  };

  // Эти операторы не должны делать никаких проверок на пустоту.
  // Проверки остаются на совести программиста.
  T& operator*() {
      return *(reinterpret_cast<T*>(data));
  };
  
  const T& operator*() const {
      return *(reinterpret_cast<const T*>(data));
  };
  
  T* operator->() {
      return reinterpret_cast<T*>(data);
  };
  
  const T* operator->() const {
      return reinterpret_cast<const T*>(data);
  };

  // Генерирует исключение BadOptionalAccess, если объекта нет
  T& Value() {
      if (defined) {
	  return *(reinterpret_cast<T*>(data));
      }
      else {
	  throw BadOptionalAccess("Optional does not have value");
      }
  };
  const T& Value() const {
      if (defined) {
	  return *(reinterpret_cast<const T*>(data));
      }
      else {
	  throw BadOptionalAccess("Empty Value");
      }
  };

  void Reset() {
      if (defined) {
	  reinterpret_cast<T*>(data)->~T();
      }
      defined = false;
  };

  ~Optional() {
      Reset();
  };
};
