#pragma once

#include <cmath>
#include <map>
#include <set>
#include <string>

class Stop {
public:    
    explicit Stop(const std::string& name,
		  double latitude,
		  double longitude,
		  const std::map<std::string, int>& distances,
	          std::size_t id) :
        areCoordinatesSet_(true),
	name_(name),
	latitude_(latitude * 3.1415926535/180.),
	longitude_(longitude * 3.1415926535/180.),
	distances_(move(distances)),
	id_(id) {};
	
	explicit Stop(const std::string& name, std::size_t id) :
        areCoordinatesSet_(false),
	name_(name),
	latitude_(0.),
	longitude_(0.),
	id_(id) {};

    void SetProperties(double lat,
		       double lon,
		       const std::map<std::string, int>& distances);
    bool isSet() const;
    double CalculateDistance(const Stop& other) const;
    void AddRoute(const std::string& route_name);
    const std::set<std::string>& GetRoutes() const;
    const std::string& GetName() const;
    std::size_t GetId() const;
    const std::map<std::string, int>& GetDistances() const;
    std::optional<double> GetDistanceTo(const std::string& name);
    void Print() const; 
    
private:
    
    bool areCoordinatesSet_;
    std::string name_;
    double latitude_;
    double longitude_;
    std::set<std::string> routes_;
    std::map<std::string, int> distances_;
    std::size_t id_;
};
