#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include "graph.h"
#include "json.h"
#include "router.h"
#include "stop.h"
#include "svg.h"
#include "transport_manager.h"

using namespace Graph;
using namespace Json;
using namespace std;

shared_ptr<Stop> TransportManager::AddStop(const string& name,
					   double lat,
					   double lon,
					   const map<string, int>& distances) {
    shared_ptr<Stop> result = nullptr;
    
    auto it = stops.find(name);
    if (it != stops.end()) {
	if (!it->second->isSet()) {
	    it->second->SetProperties(lat, lon, distances);
	}
	result = it->second;
    }
    else {
	result = make_shared<Stop>(name, lat, lon, distances, stops.size());
	stops[name] = result;
	stops_id[result->GetId()] = result;
    }

    return result;
}

shared_ptr<Stop> TransportManager::AddStop(const string& name,
					   const string& route_name) {
    shared_ptr<Stop> result = nullptr;
    
    auto it = stops.find(name);
    if (it != stops.end()) {
	result = it->second;
    }
    else {
	result = make_shared<Stop>(name, stops.size());
	stops[name] = result;
	stops_id[result->GetId()] = result;
    }

    result->AddRoute(route_name);

    return result;
}

void TransportManager::AddRoute(const string& name,
				const vector<string>& stops,
				bool isCircular) {
    auto it = routes.find(name);
    
    if (it == routes.end()) {
	routes[name] = make_shared<Route>(name, isCircular);
	for (const auto& stop_name : stops) {
	    routes[name]->AddStop(AddStop(stop_name, name));
	}
    }
}

ostream& TransportManager::DescribeRoute(const string& name,
					 ostream& out) const {
    auto it = routes.find(name);
    if (it == routes.end()) {
	out << "Bus " << name << ": not found";
    }
    else {
	double length = it->second->CalculateLength();
	int curved = it->second->CalculateCurvedLength();
	double curvature = curved/length;
	out << fixed << setprecision(6)
	    << "Bus " << name << ": " << it->second->GetNumberOfStops()
	    << " stops on route, " << it->second->GetNumberOfUniqueStops()
	    << " unique stops, " << curved << " route length, "
	    << curvature << " curvature";
    }
    return out;
}

Node TransportManager::DescribeRouteJSON(const string& name,
					 int id) const {
    map<string, Node> answer;
    answer["request_id"] = Node(id);
    
    auto it = routes.find(name);
    if (it == routes.end()) {
	answer["error_message"] = Node(string("not found"));
    }
    else {
	auto route = it->second;
	double length = route->CalculateLength();
	int curved = route->CalculateCurvedLength();
	double curvature = curved/length;
	answer["route_length"] = Node(curved);
	answer["curvature"] = Node(curvature);
	answer["stop_count"] = Node(route->GetNumberOfStops());
	answer["unique_stop_count"] = Node(route->GetNumberOfUniqueStops());
    }
    return Node(answer);
}

ostream& TransportManager::DescribeStop(const string& name,
					ostream& out) const {
    auto it = stops.find(name);

    if (it == stops.end()) {
	out << "Stop " << name << ": not found";
    }
    else {
	out << "Stop " << name << ": ";
	auto rs = it->second->GetRoutes();

	if (rs.empty()) {
	    out << "no buses";
	}
	else {
	    out << "buses ";
	    auto it_ = rs.begin();
	    for (; it_ != prev(rs.end()); ++it_) {
		out << *it_ << " ";
	    }
	    out << *it_;
	}
    }
    return out;
}

Node TransportManager::DescribeStopJSON(const string& name, int id) const {
    auto it = stops.find(name);
    map<string, Node> answer;
    
    answer["request_id"] = Node(id);
    
    if (it == stops.end()) {
	answer["error_message"] = Node(string("not found"));
    }
    else {
	auto rs = it->second->GetRoutes();

	if (rs.empty()) {
	    answer["buses"] = Node(vector<Node>());
	}
	else {
	    vector<Node> buffer;
	    buffer.reserve(rs.size());
	     
	    for (const auto& s : rs) {
		buffer.push_back(Node(s));
	    }
	    answer["buses"] = Node(buffer);
	}
    }

    return Node(answer);
}

Node TransportManager::DescribePathStopJSON(const string& stop_name) const {
    map<string, Node> item;
    item["type"] = Node(string("Wait"));
    item["time"] = Node(wait_time);
    item["stop_name"] = Node(stop_name);
    return Node(item);
}

Node TransportManager::DescribePathBusJSON(const string& bus, int span, double time) const {
    map<string, Node> item;
    item["type"] = Node(string("Bus"));
    item["span_count"] = Node(span);
    item["bus"] = Node(bus);
    item["time"] = Node(time);
    return Node(item);
}

Node TransportManager::DescribePathJSON(const string& from,
					const string& to,
					int id) const {
    if (router == nullptr) {
	graph = make_shared<DirectedWeightedGraph<double>>(stops.size());
	for (const auto& [name, route_ptr] : routes) {
	    CreateEdgesForRoute(graph, route_ptr);
	}
	//cout << "Vertex " << graph->GetVertexCount()
	//    << " Edges " << graph->GetEdgeCount() << endl;
	router = make_shared<Router<double>>(*graph);
    }

    map<string, Node> answer;
    answer["request_id"] = Node(id);
    
    auto fromId = stops.at(from)->GetId();
    auto toId = stops.at(to)->GetId();
    
    auto route = router->BuildRoute(fromId, toId);
    
    if (!route) {
	answer["error_message"] = Node(string("not found"));
    }
    else {
	//cout << "id: " << id << " Route Id " << route->id
	//   << " time " << route->weight
	//   << " edge_count " << route->edge_count << endl;
	vector<Node> buffer;
	double total_time = 0.;
	for (int i = 0; i < route->edge_count; ++i) {
	    auto edgeId = router->GetRouteEdge(route->id, i);
	    auto& edge = edges.at(edgeId);
	    
	    auto& fromStop = stops_id.at(edge.from);
	    //auto& toStop = stops_id.at(edge.to);
	    auto time = edge.time;
	    auto span = edge.span;
	    auto& name = edge.name;
	    total_time += time;
	    
	    buffer.push_back(DescribePathStopJSON(fromStop->GetName()));
	    buffer.push_back(DescribePathBusJSON(name, span, time - wait_time));
	    //cout << "Part " << i
	    //	 << ": from " << fromStop->GetName()
	    //	 << " to " << toStop->GetName()
	    //	 << " Time: " << time
	    //	 << " Span: " << span << endl;
	    
	}
	answer["total_time"] = Node(total_time);
	answer["items"] = Node(move(buffer));
    }
    return Node(answer);
    
}

Svg::Point TransportManager::GetStopCoords(shared_ptr<Stop> stop) const {
    /*double lon = stop->GetLongitude();
    double lat = stop->GetLatitude();
    double x = (lon - render.min_lon) * render.zoom_coeff + render.padding;
    double y = (render.max_lat - lat) * render.zoom_coeff + render.padding;
    return {x, y};*/
/*    if (stop->GetX() <= 7794.743290
	&& stop->GetX() >= 7794.743288) {
	//cerr << input_ << endl;
	//size_t pos = input_.find(", \"dK5ymJu4cTLY2RFhJv9RDPUjJ\": 226620");
	//size_t pos = input_.find(", \"jagWL786Ig\": 981945");
	//size_t pos = input_.find(", \"7mHaYV3Kjho6u0hkXF2\"");
	//size_t pos = input_.find(", \"name\": \"Yy2poEi ZhVl4Q\"");
	//size_t pos = input_.find(", \"road_distances\": {\"AVivHu1WNWZM1VvTye\": 736760");
	//size_t pos = input_.find(", \"xkN6KMPmlkUO9iRo\": 604361");
	//size_t pos = input_.find(", \"stops\": [\"tWuOh3y0f98adADHNxA1wD\"");
	//size_t pos = input_.find(", \"stops\": [\"QaOZyLMjM\"");
	//size_t pos = input_.find(", \"DdlDe8chaPdQIrCe\": 535492");
	//size_t pos = input_.find(", \"ysNQlFHxW7S7PO\": 923631");
	//size_t pos = input_.find(", \"stops\": [\"QaOZyLMjM\"");
	//size_t pos = input_.find(", \"DdlDe8chaPdQIrCe\": 535492");
	//size_t pos = input_.find(", \"ysNQlFHxW7S7PO\": 923631");
	//throw runtime_error(input_.substr(pos, string::npos));
	throw runtime_error(to_string(routes.size()));
	}*/
    return {stop->GetX(), stop->GetY()};
}

bool TransportManager::IfStopsAdjacent(shared_ptr<Stop> a, shared_ptr<Stop> b) const {
    auto routesA = a->GetRoutes();
    /*cout << "Enter "
	 << a->GetName() << " "
	 << b->GetName() << endl;*/
    for (const auto& route_name : routesA) {
	auto route = routes.at(route_name);
	const auto& stops_for_route = route->GetStops();
	/*cout << "Route "<< route->GetName() << endl;
	for (auto stop_ : stops_for_route) {
	    cout << stop_->GetName() << endl;
	}
	cout << "-------" << endl;*/
	for (auto it = stops_for_route.begin();
	          it != stops_for_route.end(); ++it) {
	    if (*it == a) {
		if (it != prev(stops_for_route.end())) {
		    if (*next(it) == b) return true;
		}

		if (it != stops_for_route.begin()) {
		    if (*prev(it) == b) return true;
		}	
	    }
	}
	
	/*{
	    auto stopAIt = find(stops_for_route.begin(),
				stops_for_route.end(), a);
	    		
	    if (stopAIt != prev(stops_for_route.end())) {
		if (*next(stopAIt) == b) return true;
	    }

	    if (stopAIt != stops_for_route.begin()) {
		if (*prev(stopAIt) == b) return true;
	    }
	}
	
	{
	    auto stopAIt = find(stops_for_route.rbegin(),
				stops_for_route.rend(), a);
	    
	    if (stopAIt != prev(stops_for_route.rend())) {
		if (*next(stopAIt) == b) return true;
	    }

	    if (stopAIt != stops_for_route.rbegin()) {
		if (*prev(stopAIt) == b) return true;
	    }
	    }*/
    }
    
    return false;
};

void TransportManager::UpdateRenderParameters() const {
//    double min_lon = numeric_limits<double>::max();
//    double max_lon = numeric_limits<double>::min();
//    double min_lat = numeric_limits<double>::max();
//    double max_lat = numeric_limits<double>::min();

    vector<shared_ptr<Stop>> sorted_stops;
	
    for (const auto& p : stops) {
//	double lon = p.second->GetLongitude();
//	double lat = p.second->GetLatitude();
//	min_lon = min(min_lon, lon);
//	max_lon = max(max_lon, lon);
//	min_lat = min(min_lat, lat);
//	max_lat = max(max_lat, lat);
	sorted_stops.push_back(p.second);
    }

    int count = sorted_stops.size();
    
    if (count == 1) {
	sorted_stops[0]->SetX(render.padding);
	sorted_stops[0]->SetY(render.height - render.padding);
    }
    else {
	sort(sorted_stops.begin(), sorted_stops.end(),
	     [](auto lhs, auto rhs) {
		 return lhs->GetLongitude() < rhs->GetLongitude();});
	//cout << "First pass" << endl;
	vector<shared_ptr<Stop>> prev_list;
    	int idx = 0;
	auto curr = sorted_stops[idx];
	curr->SetX(idx);
	
	for (int i = 1; i < count; ++i) {
	    prev_list.push_back(curr);
	    curr = sorted_stops[i];

	    for (auto prev : prev_list) {
		if (IfStopsAdjacent(curr, prev)) {
		    /*cout << "Adjacent: "
			 << curr->GetName() << " "
		    	 << prev->GetName() << endl;*/
		    idx++;
		    prev_list.clear();
		    break;
		}
	    }
	    curr->SetX(idx);
	}
	
        double x_step =
	    (idx > 0) ? (render.width - 2 * render.padding) / idx : 0.;
	
	for (int i = 0; i < count; ++i) {
	    curr = sorted_stops[i];
	    curr->SetX(curr->GetX() * x_step + render.padding);
	}
	
	sort(sorted_stops.begin(), sorted_stops.end(),
	     [](auto lhs, auto rhs) {
		 return lhs->GetLatitude() < rhs->GetLatitude();});

	//cout << "Second pass" << endl;
	
	idx = 0;
	curr = sorted_stops[idx];
	curr->SetY(idx);
	prev_list.clear();
	
	for (int i = 1; i < count; ++i) {
	    prev_list.push_back(curr);
	    curr = sorted_stops[i];
	    
	    for (auto prev : prev_list) {
		if (IfStopsAdjacent(curr, prev)) {
		    /*cout << "Adjacent: "
		         << curr->GetName() << " "
		    	 << prev->GetName() << endl;*/
		    idx++;
		    prev_list.clear();
		    break;
		}
	    }
	    curr->SetY(idx);
	}
	
	double y_step =
	    (idx > 0) ? (render.height - 2 * render.padding) / idx : 0.;
	
	for (int i = 0; i < count; ++i) {
	    curr = sorted_stops[i];
	    curr->SetY(render.height - render.padding - curr->GetY() * y_step);
	}
    }
    
/*    if (max_lon == min_lon && max_lat == min_lat) {
	render.zoom_coeff = 0.;
    }
    else if (max_lon == min_lon) {
	render.zoom_coeff = (render.height - 2 * render.padding) /
	    (max_lat - min_lat);
    }
    else if (max_lat == min_lat) {
	render.zoom_coeff = (render.width - 2 * render.padding) /
	    (max_lon - min_lon);
    }
    else {
	double width_zoom_coef = (render.width - 2 * render.padding) /
	    (max_lon - min_lon);
	double height_zoom_coef = (render.height - 2 * render.padding) /
	    (max_lat - min_lat);
	render.zoom_coeff = min(width_zoom_coef, height_zoom_coef);
    }
    render.min_lon = min_lon;
    render.max_lat = max_lat;*/
}

void TransportManager::PlotRoutes(Svg::Document& map) const {
    int idx = 0;
    int number_of_colors = render.color_palette.size();
    for (const auto& r : routes) {
	auto route = r.second;
	
	Svg::Polyline line;
	line.SetStrokeColor(render.color_palette[idx % number_of_colors])  
	    .SetStrokeWidth(render.line_width)
	    .SetStrokeLineCap("round")
	    .SetStrokeLineJoin("round");
	
	auto& stops = route->GetStops();
	for (auto it = stops.begin(); it != stops.end(); ++it) {
	    line.AddPoint(GetStopCoords(*it));
	}
	if (!route->IsCircular()) {
	    for (auto it = next(stops.rbegin()); it != stops.rend(); ++it) {
		line.AddPoint(GetStopCoords(*it));
	    }  
	}
	map.Add(line);
	++idx;
    }
}

void TransportManager::PlotSingleRouteName(Svg::Document& map,
					   shared_ptr<Stop> stop,
					   string route,
					   int idx) const {
    Svg::Point point = GetStopCoords(stop);
	
    Svg::Text fill;
    fill.SetPoint(point)
	.SetOffset(render.bus_label_offset)
	.SetFontSize(render.bus_label_font_size)
	.SetFontFamily("Verdana")
	.SetFontWeight("bold")
	.SetData(route)
	.SetStrokeColor(render.underlayer_color)
	.SetFillColor(render.underlayer_color)
	.SetStrokeWidth(render.underlayer_width)
	.SetStrokeLineCap("round")
	.SetStrokeLineJoin("round");
    map.Add(fill);

    Svg::Text name;
    name.SetPoint(point)
	.SetOffset(render.bus_label_offset)
	.SetFontSize(render.bus_label_font_size)
	.SetFontFamily("Verdana")
	.SetFontWeight("bold")
	.SetData(route)
	.SetFillColor(render.color_palette[idx]);
    map.Add(name);
}

void TransportManager::PlotRouteNames(Svg::Document& map) const {
    int idx = 0;
    int number_of_colors = render.color_palette.size();
    for (const auto& r : routes) {
	idx %= number_of_colors;
	
	auto route = r.second;
	auto first_stop = route->GetStops().front();
	auto last_stop = route->GetStops().back();
	PlotSingleRouteName(map, first_stop, r.first, idx);

	if (!route->IsCircular() && first_stop != last_stop) {
	    PlotSingleRouteName(map, last_stop, r.first, idx);
	}
	++idx;
    }
}

void TransportManager::PlotStops(Svg::Document& map) const {
    for (const auto& s : stops) {
	Svg::Circle circle;
	circle.SetFillColor("white")
	    .SetRadius(render.stop_radius)
	    .SetCenter(GetStopCoords(s.second));
	map.Add(circle);
    }
}

void TransportManager::PlotStopNames(Svg::Document& map) const {
    for (const auto& s : stops) {
	auto stop = s.second;
	Svg::Point point = GetStopCoords(stop);
	
	Svg::Text fill;
	fill.SetPoint(point)
	    .SetOffset(render.stop_label_offset)
	    .SetFontSize(render.stop_label_font_size)
	    .SetFontFamily("Verdana")
	    .SetData(s.first)
	    .SetStrokeColor(render.underlayer_color)
	    .SetFillColor(render.underlayer_color)
	    .SetStrokeWidth(render.underlayer_width)
	    .SetStrokeLineCap("round")
	    .SetStrokeLineJoin("round");
	map.Add(fill);

	Svg::Text name;
	name.SetPoint(point)
	    .SetOffset(render.stop_label_offset)
	    .SetFontSize(render.stop_label_font_size)
	    .SetFontFamily("Verdana")
	    .SetData(s.first)
	    .SetFillColor("black");
	map.Add(name);
    }
}

Node TransportManager::CreateMap(int id) const {
    map<string, Node> answer;
    Svg::Document map;
    stringstream out;
    
    UpdateRenderParameters();
    for (const auto& layer : render.layers) {
	if (layer == "bus_lines") {
	    PlotRoutes(map);
	}
	else if (layer == "bus_labels") {
	    PlotRouteNames(map);
	}
	else if (layer == "stop_points") {
	    PlotStops(map);
	}
	else if (layer == "stop_labels") {
	    PlotStopNames(map);
	}
    }
    
    map.RenderJSON(out);

    ofstream svg_out("map.svg", ios::out);
    map.Render(svg_out);
    
    answer["request_id"] = Node(id);
    answer["map"] = Node(out.str());
    return answer;
}

void TransportManager::CreateEdgesForRoute(
	shared_ptr<DirectedWeightedGraph<double>> graph,
	shared_ptr<Route> route) const {
    if (route->IsCircular()) {
	CreateEdgesForCircularRoute(graph, route);
    }
    else {
	CreateEdgesForLinearRoute(graph, route);
    }
}
  
void TransportManager::CreateEdgesForLinearRoute(
	shared_ptr<DirectedWeightedGraph<double>> graph,
	shared_ptr<Route> route) const {
    auto& stops = route->GetStops();
    auto& name = route->GetName();
    
    for (int i = 0; i < stops.size(); ++i) {
	double forward_time = wait_time; // waiting time is included
	double backward_time = wait_time; // waiting time is included
	int forward_span = 0;
	int backward_span = 0;
	auto from = stops[i];
	auto from_id = from->GetId();
	//auto& from_name = from->GetName();
	
	for (int j = i+1; j < stops.size(); ++j) {
	    auto to = stops[j];
	    auto to_id = to->GetId();
	    auto& to_name = to->GetName();
	    forward_span++;
	    auto dist = stops[j-1]->GetDistanceTo(to_name);
	    if (!dist) {
		dist = to->GetDistanceTo(stops[j-1]->GetName());
	    }
	    //cout << name << "F Distance from " << from_name
	    //	 << " to " << to_name << " " << *dist << endl;
	    forward_time += *dist/velocity;
	    auto edge_id = graph->AddEdge({from_id, to_id, forward_time});
	    edges[edge_id] = {from_id, to_id, name, forward_time, forward_span};
	}
	
	for (int j = i-1; j >= 0; --j) {
	    auto to = stops[j];
	    auto to_id = to->GetId();
	    auto& to_name = to->GetName();
	    backward_span++;
	    auto dist = stops[j+1]->GetDistanceTo(to_name);
	    if (!dist) {
		dist = to->GetDistanceTo(stops[j+1]->GetName());
	    }
	    //cout << name << "B Distance from " << from_name
	    //<< " to " << to_name << " " << *dist << endl;
	    backward_time += *dist/velocity;
	    auto edge_id = graph->AddEdge({from_id, to_id, backward_time});
	    edges[edge_id] = {from_id, to_id, name, backward_time, backward_span};
	}
    }
}

void TransportManager::CreateEdgesForCircularRoute(
	shared_ptr<DirectedWeightedGraph<double>> graph,
	shared_ptr<Route> route) const {
    auto& stops = route->GetStops();
    auto& name = route->GetName();

    for (int i = 0; i < stops.size() - 1; ++i) {
	double forward_time = 1.*wait_time; // waiting time is included
	int forward_span = 0;
	auto from = stops[i];
	for (int j = i+1; j < stops.size(); ++j) {
	    auto to = stops[j];
	    auto from_id = from->GetId();
	    
	    auto to_id = to->GetId();
	    auto& to_name = to->GetName();
	    forward_span++;
	    auto dist = stops[j-1]->GetDistanceTo(to_name);
	    if (!dist) {
		dist = to->GetDistanceTo(stops[j-1]->GetName());
	    }
	    //cout << "Distance from " << from_name
	    //	 << " to " << to_name << " " << *dist << endl;
	    forward_time += *dist/velocity;
	    auto edge_id = graph->AddEdge({from_id, to_id, forward_time});
	    edges[edge_id] = {from_id, to_id, name, forward_time, forward_span};
	}
    }
}

void TransportManager::SetWaitTime(int wt) {
    wait_time = wt;
}

void TransportManager::SetVelocity(double v) {
    velocity = v*1000./60.; // convert km/h to m/min
}

int TransportManager::GetWaitTime() const {
    return wait_time;
}

double TransportManager::GetVelocity() const {
    return velocity;
}

void TransportManager::SetRenderParameters(const RenderParameters& p) {
    render = p;
};

void TransportManager::Print() const {
    cout << "Stops" << endl;
    for (const auto& p : stops) {
	p.second->Print();
    }

    cout << "Routes" << endl;
    for (const auto& p : routes) {
	p.second->Print();
    }
}
