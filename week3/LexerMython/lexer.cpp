#include "lexer.h"
#include <iostream>
#include <algorithm>
#include <charconv>
#include <unordered_map>

using namespace std;

namespace Parse {

const unordered_set<char> Lexer::special_ = {'+', '-', '*', '/',
					     '(', ')', '.', ',',
					     '!', '=', '>', '<',
					     ':', '?'};
						   
bool operator == (const Token& lhs, const Token& rhs) {
  using namespace TokenType;

  if (lhs.index() != rhs.index()) {
    return false;
  }
  if (lhs.Is<Char>()) {
    return lhs.As<Char>().value == rhs.As<Char>().value;
  } else if (lhs.Is<Number>()) {
    return lhs.As<Number>().value == rhs.As<Number>().value;
  } else if (lhs.Is<String>()) {
    return lhs.As<String>().value == rhs.As<String>().value;
  } else if (lhs.Is<Id>()) {
    return lhs.As<Id>().value == rhs.As<Id>().value;
  } else {
    return true;
  }
}

std::ostream& operator << (std::ostream& os, const Token& rhs) {
  using namespace TokenType;

#define VALUED_OUTPUT(type) \
  if (auto p = rhs.TryAs<type>()) return os << #type << '{' << p->value << '}';

  VALUED_OUTPUT(Number);
  VALUED_OUTPUT(Id);
  VALUED_OUTPUT(String);
  VALUED_OUTPUT(Char);

#undef VALUED_OUTPUT

#define UNVALUED_OUTPUT(type) \
    if (rhs.Is<type>()) return os << #type;

  UNVALUED_OUTPUT(Class);
  UNVALUED_OUTPUT(Return);
  UNVALUED_OUTPUT(If);
  UNVALUED_OUTPUT(Else);
  UNVALUED_OUTPUT(Def);
  UNVALUED_OUTPUT(Newline);
  UNVALUED_OUTPUT(Print);
  UNVALUED_OUTPUT(Indent);
  UNVALUED_OUTPUT(Dedent);
  UNVALUED_OUTPUT(And);
  UNVALUED_OUTPUT(Or);
  UNVALUED_OUTPUT(Not);
  UNVALUED_OUTPUT(Eq);
  UNVALUED_OUTPUT(NotEq);
  UNVALUED_OUTPUT(LessOrEq);
  UNVALUED_OUTPUT(GreaterOrEq);
  UNVALUED_OUTPUT(None);
  UNVALUED_OUTPUT(True);
  UNVALUED_OUTPUT(False);
  UNVALUED_OUTPUT(Eof);

#undef UNVALUED_OUTPUT

  return os << "Unknown token :(";
}


Lexer::Lexer(std::istream& input) : input_(input) {
    NextToken();
}

const Token& Lexer::CurrentToken() const {
    return *currentToken_;
}

Token Lexer::NextToken() {
    /* Check if already at the end of file */
    if (currentToken_ &&
	CurrentToken().Is<TokenType::Eof>()) {
	return *currentToken_;
    }
    
    /* check if encountered end of file */
    if (auto eof = ParseEof(); eof) {
	currentToken_ = *eof;
	return *currentToken_;
    }
      
    optional<Token> token;
    do {
	if (token = ParseIndent(); token) {
	    break;
	}
	
	SkipSpaces();
	token = ParseToken();}
    while (!token);

    currentToken_ = *token;
    return *currentToken_;
}
    
optional<Token> Lexer::ParseToken() {
    char c = input_.peek();

    if (auto nwl = ParseNewline(c); nwl) {
	if (currentToken_ &&
	    !CurrentToken().Is<TokenType::Newline>()) {
	    return *nwl;
	}
	else {
	    /* empty string is ignored */
	    return nullopt;
	}
    }
  
    if (auto str = ParseString(c); str) {
	return *str;
    }

    if (auto chr = ParseSpecial(c); chr) {
	return *chr;
    }
    
    if (auto gen = ParseGeneralToken(c); gen) {
	return *gen;
    }
    return nullopt;
}

optional<Token> Lexer::ParseIndent() {
    /* check indentation:
    1. At the beginning of file
    2. After new line
    3. After Dedent, because there can be several Dedent */
    if (!currentToken_ ||
	CurrentToken().Is<TokenType::Newline>() ||
	CurrentToken().Is<TokenType::Dedent>()) {
	bool isDedent = currentToken_ &&
	  	        CurrentToken().Is<TokenType::Dedent>();
	if (indentChange_ == 0 && !isDedent)
	{
	    int indent = 0;
	    char c = input_.peek();
	    while (c == ' ') {
		input_.get();
		indent++;
		c = input_.peek();
	    }
	
	    if (c != '\n') { // not simply empty string with newline
		indentChange_ = (indent - indent_)/2;
	    }
	}
    
	if (indentChange_ < 0) {
	    indentChange_++;
	    indent_ -= 2;
	    return TokenType::Dedent{};
	}
	else if (indentChange_ > 0) {
	    indentChange_--;
	    indent_ += 2;
	    return TokenType::Indent{};
	}
    }
    return nullopt;
}

void Lexer::SkipSpaces() {
    char c = input_.peek();
    while (c == ' ') {
	input_.get();
	c = input_.peek();
    }
}

bool Lexer::IsEof() {
    input_.peek();
    return input_.eof();
}

optional<int> Lexer::IsNumber(const string& s) {
    char* p;
    if (s.size() == 0) {
	return nullopt;
    }
    
    int	converted = strtol(s.data(), &p, 10);
    
    if (*p) {
	return nullopt;
    }
    else {
	return converted;
    }
}

optional<Token> Lexer::ParseString(char c) {
    string token;
    
    if (c == '"') {
	input_.get(); //extract peeked char
	c = input_.get();
	while (c != '"') {
	   token.push_back(c);
	   c = input_.get(); 
	}
	return TokenType::String{token};
    }

    if (c == '\'') {
	input_.get(); //extract peeked char
	c = input_.get();
	while (c != '\'') {
	    token.push_back(c);
	    c = input_.get(); 
	}
	return TokenType::String{token};
    }
    return nullopt;
}

optional<Token> Lexer::ParseSpecial(char c) {
    if (special_.count(c) != 0) {
	c = input_.get();
	if (c == '>' || c == '<' || c == '=') {
	    char n = input_.peek();
	    if (n != '=') {
	        return TokenType::Char{c};
	    }
	    else {
		input_.get();
		if (c == '=') {
		    return TokenType::Eq{};
		}
		else if (c == '>') {
		    return TokenType::GreaterOrEq{};
		}
		else if (c == '<') {
		    return TokenType::LessOrEq{};
		}
	    }
	}
	else if (c == '!') {
	    char n = input_.peek();
	    if (n == '=') {
		input_.get();
	        return TokenType::NotEq{};
	    }
	}
	else {
	    return TokenType::Char{c};
	}
    }
    return nullopt;
}

optional<Token> Lexer::ParseGeneralToken(char c) {
    string token;
    while (!input_.eof() &&
	   c != ' '      &&
           c != '\n'     &&
	   special_.count(c) == 0) {
	c = input_.get();
	token.push_back(c);
	c = input_.peek();
    }
   
    if (token == "class") {
	return TokenType::Class{};
    }
    else if (token == "return") {
	return TokenType::Return{};
    }
    else if (token == "if") {
	return TokenType::If{};
    }
    else if (token == "else") {
	return TokenType::Else{};
    }
    else if (token == "def") {
	return TokenType::Def{};
    }
    else if (token == "print") {
	return TokenType::Print{};
    }
    else if (token == "or") {
	return TokenType::Or{};
    }
    else if (token == "None") {
	return TokenType::None{};
    }
    else if (token == "and") {
	return TokenType::And{};
    }
    else if (token == "not") {
	return TokenType::Not{};
    }
    else if (token == "True") {
	return TokenType::True{};
    }
    else if (token == "False") {
	return TokenType::False{};
    }
    else {
	auto value = IsNumber(token);
	if (!value) {
	    return TokenType::Id{token};
	}
	else {
	    return TokenType::Number{*value};
	}
    }
    return nullopt;
}

optional<Token> Lexer::ParseNewline(char c) {
    if (c == '\n') {
 	input_.get();
	return TokenType::Newline{};
    }
    return nullopt;
}

optional<Token> Lexer::ParseEof() {
    if (IsEof()) {
	if (currentToken_ && 
	    (CurrentToken().Is<TokenType::Newline>() ||
	     CurrentToken().Is<TokenType::Dedent>())) {
	    /* If already newline, insert eof */
	    return TokenType::Eof{};
	}
	/* First insert Newline */
	return TokenType::Newline{};
    }
    return nullopt;
}

} /* namespace Parse */
