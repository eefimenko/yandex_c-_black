#include <iomanip>
#include <sstream>

#include "json.h"
#include "request.h"

using namespace std;
using namespace Json;

void ReadRouteRequest::ParseFromJSON(const Node& node) {
    name = node.AsMap().at("name").AsString();
    id = node.AsMap().at("id").AsInt();
}

Node ReadRouteRequest::ProcessJSON(const TransportManager& manager) const {
    return manager.DescribeRouteJSON(name, id);
}

void ReadStopRequest::ParseFromJSON(const Node& node) {
    name = node.AsMap().at("name").AsString();
    id = node.AsMap().at("id").AsInt();
}

Node ReadStopRequest::ProcessJSON(const TransportManager& manager) const{
    return manager.DescribeStopJSON(name, id);
}

void ReadMapRequest::ParseFromJSON(const Node& node) {
    id = node.AsMap().at("id").AsInt();
}

Node ReadMapRequest::ProcessJSON(const TransportManager& manager) const{
    //manager.Print();
    return manager.CreateMap(id);
}

void CalcPathRequest::ParseFromJSON(const Node& node) {
    from = node.AsMap().at("from").AsString();
    to = node.AsMap().at("to").AsString();
    id = node.AsMap().at("id").AsInt();
}

Node CalcPathRequest::ProcessJSON(const TransportManager& manager) const{
    return manager.DescribePathJSON(from, to, id);
}

void AddStopRequest::ParseFromJSON(const Node& node) {
    name = node.AsMap().at("name").AsString();
    for (const auto& n: node.AsMap().at("road_distances").AsMap()) {
	distances[n.first] = n.second.AsInt();
    }
    lat = node.AsMap().at("latitude").AsDouble();
    lon = node.AsMap().at("longitude").AsDouble();
}

void AddStopRequest::ProcessJSON(TransportManager& manager) const {
    manager.AddStop(name, lat, lon, distances);
}

void AddRouteRequest::ParseFromJSON(const Node& node) {
    name = node.AsMap().at("name").AsString();
    isCircular = node.AsMap().at("is_roundtrip").AsBool();
    for (const auto& n: node.AsMap().at("stops").AsArray()) {
	stops.push_back(n.AsString());
    }
}

void AddRouteRequest::ProcessJSON(TransportManager& manager) const {
    manager.AddRoute(name, stops, isCircular);
}

void SetParametersRequest::ParseFromJSON(const Node& node) {
    wait_time = node.AsMap().at("bus_wait_time").AsInt();
    velocity = node.AsMap().at("bus_velocity").AsDouble();
}

void SetParametersRequest::ProcessJSON(TransportManager& manager) const {
    manager.SetWaitTime(wait_time);
    manager.SetVelocity(velocity);
}

void SetRenderRequest::ParseFromJSON(const Node& node) {
    auto& map = node.AsMap();
    render.width = map.at("width").AsDouble();
    render.height = map.at("height").AsDouble();
    render.padding = map.at("padding").AsDouble();
    render.stop_radius = map.at("stop_radius").AsDouble();
    render.line_width = map.at("line_width").AsDouble();
    render.outer_margin = map.at("outer_margin").AsDouble();
    render.stop_label_font_size = map.at("stop_label_font_size").AsInt();
    render.bus_label_font_size = map.at("bus_label_font_size").AsInt();
    render.underlayer_width = map.at("underlayer_width").AsDouble();
    render.stop_label_offset = map.at("stop_label_offset");
    render.bus_label_offset = map.at("bus_label_offset");
    render.underlayer_color = map.at("underlayer_color");
    //cout << render.underlayer_color.ToString() << endl;
    auto& palette = map.at("color_palette").AsArray();
    for (const auto& color : palette) {
	render.color_palette.emplace_back(color);	
    }
    auto& layers = map.at("layers").AsArray();
    for (const auto& layer : layers) {
	render.layers.push_back(layer.AsString());	
    }
/*    for (const auto& color : render.color_palette) {
	cout << color.ToString() << endl; 
	}*/
}

void SetRenderRequest::ProcessJSON(TransportManager& manager) const {
    manager.SetRenderParameters(render);
}

vector<RequestHolder> ReadRequestsJSON(istream& in_stream, string& input) {
    vector<RequestHolder> requests;
    stringstream out;
    
    auto doc = Load(in_stream);
    Print(doc, out);
    //cout << out.str() << endl;
    input = out.str();
    
    auto& req = doc.GetRoot().AsMap();
    
    
    {
	const string settings_type = "routing_settings";
	const auto& settings = req.at(settings_type);
    
	RequestHolder request = Request::Create(Request::Type::SET_PARAM);
	if (request) {
	    request->ParseFromJSON(settings);
	};
	requests.push_back(move(request));
    }
    
    {
	const string settings_type = "render_settings";
	const auto& settings = req.at(settings_type);
    
	RequestHolder request = Request::Create(Request::Type::SET_RENDER);
	if (request) {
	    request->ParseFromJSON(settings);
	};
	requests.push_back(move(request));
    }
	
    const string modify_type = "base_requests";
    const auto& base_req = req.at(modify_type).AsArray();

    for (const auto& node : base_req) {
	string type = node.AsMap().at("type").AsString();
	Request::Type rtype;
	if (type == "Stop") {
	    rtype = Request::Type::ADD_STOP;
	}
	else if (type == "Bus") {
	    rtype = Request::Type::ADD_ROUTE;
	}
	else {
	    continue;
	}
	RequestHolder request = Request::Create(rtype);
	
	if (request) {
	    request->ParseFromJSON(node);
	};
	requests.push_back(move(request));
    }

    const string read_type = "stat_requests";
    const auto& stat_req = req.at(read_type).AsArray();

    for (const auto& node : stat_req) {
	string type = node.AsMap().at("type").AsString();
	Request::Type rtype;
	if (type == "Stop") {
	    rtype = Request::Type::READ_STOP;
	}
	else if (type == "Bus") {
	    rtype = Request::Type::READ_ROUTE;
	}
	else if (type == "Route") {
	    rtype = Request::Type::CALC_PATH;
	}
	else if (type == "Map") {
	    rtype = Request::Type::READ_MAP;
	}
	else {
	    continue;
	}
	RequestHolder request = Request::Create(rtype);
	
	if (request) {
	    request->ParseFromJSON(node);
	};
	requests.push_back(move(request));
    }
        
    return requests;
}

vector<Node> ProcessRequestsJSON(TransportManager& manager,
				 const vector<RequestHolder>& requests,
				 const string& input) {
    manager.SetInput(input);
    vector<Node> responses;
    
    for (const auto& request_holder : requests) {
	if (request_holder->type == Request::Type::READ_ROUTE) {
	    const auto& request = static_cast<const ReadRouteRequest&>(*request_holder);
	    responses.push_back(request.ProcessJSON(manager));
	}
	else if (request_holder->type == Request::Type::READ_STOP) {
	    const auto& request = static_cast<const ReadStopRequest&>(*request_holder);
	    responses.push_back(request.ProcessJSON(manager));
	}
	else if (request_holder->type == Request::Type::CALC_PATH) {
	    const auto& request = static_cast<const CalcPathRequest&>(*request_holder);
	    responses.push_back(request.ProcessJSON(manager));
	}
	else if (request_holder->type == Request::Type::READ_MAP) {
	    const auto& request = static_cast<const ReadMapRequest&>(*request_holder);
	    responses.push_back(request.ProcessJSON(manager));
	}
	else {
	    const auto& request = static_cast<const ModifyRequest&>(*request_holder);
	    request.ProcessJSON(manager);
	}
    }
    return responses;
}

void PrintResponsesJSON(const vector<Node>& responses,
			ostream& stream) {
    stream << fixed << setprecision(16);
    PrintNode(responses, stream);
}
