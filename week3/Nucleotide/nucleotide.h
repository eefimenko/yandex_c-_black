#pragma once
#include <cstddef>
#include <cstdint>
#include <unordered_map>

struct Nucleotide {
  char Symbol;
  size_t Position;
  int ChromosomeNum;
  int GeneNum;
  bool IsMarked;
  char ServiceInfo;
};

const std::unordered_map<char, char> SymbolToIdx = {{'A', 0},{'T', 1},{'C', 2}, {'G', 3}};
const std::unordered_map<char, char> IdxToSymbol = {{0, 'A'},{1, 'T'},{2, 'C'}, {3, 'G'}};

struct CompactNucleotide {
    uint32_t Position;
    uint32_t Symbol:2;
    uint32_t ChromosomeNum:6;
    uint32_t GeneNum:15;
    uint32_t IsMarked:1;
    uint32_t ServiceInfo:8;
};

bool operator == (const Nucleotide& lhs, const Nucleotide& rhs);
CompactNucleotide Compress(const Nucleotide& n);
Nucleotide Decompress(const CompactNucleotide& cn);
