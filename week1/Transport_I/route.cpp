#include <iostream>
#include <memory>
#include <unordered_set>

#include "route.h"

using namespace std;

bool Route::IsCircular() const {
    return isCircular_;
}

const string& Route::GetName() const {
    return name_;
}

const vector<shared_ptr<Stop>>& Route::GetStops() const {
    return stops_;
}

void Route::AddStop(std::shared_ptr<Stop> stop) {
    stops_.push_back(stop);
}
    
double Route::GetLength() const {
    return length_;
}

int Route::GetNumberOfStops() const {
    return n_stops_;
}

int Route::GetNumberOfUniqueStops() const {
    return n_unique_;
}

void Route::Print() const {
    cout << "Number: " << name_ << endl;
    for (const auto& p : stops_) {
	p->Print();
    }
    cout << endl;
}

double Route::CalculateLength() {
    if (length_ == -1 && !stops_.empty()) {
	length_ = 0.;
	auto p1 = stops_.begin();
	auto p2 = next(p1);
	while (p2 != stops_.end()) {
	    length_ += p1->get()->CalculateDistance(*p2->get());
	    p1 = p2;
	    p2 = next(p2);
	}
	
	n_stops_ = stops_.size();
	if (!isCircular_) {
	    length_ *= 2.;
	    n_stops_ = 2 * n_stops_ - 1;
	}
	
	unordered_set<shared_ptr<Stop>> unique(stops_.begin(), stops_.end());
	n_unique_ = unique.size();
    }
    return length_;
}

int Route::CalculateCurvedLength() {
    if (curvedLength_ == -1 && !stops_.empty()) {
	curvedLength_ = 0.;
	double distance;

	// Forward pass
	{
	    auto p1 = stops_.begin();
	    auto p2 = next(p1);
	    while (p2 != stops_.end()) {
		auto dist = (*p1)->GetDistanceTo((*p2)->GetName());
		if (!dist) {
		    dist = (*p2)->GetDistanceTo((*p1)->GetName());
		    if (!dist) {
			distance = (*p1)->CalculateDistance(*p2->get());
		    }
		    else {
			distance = *dist; 
		    }
		}
		else {
		    distance = *dist;
		}
//		cout << distance << endl;
		curvedLength_ += distance;
		p1 = p2;
		p2 = next(p2);
	    }
	}

	// Backward pass for linear route
	if (!isCircular_) {
	    {
		auto p1 = stops_.rbegin();
		auto p2 = next(p1);
		while (p2 != stops_.rend()) {
		    auto dist = (*p1)->GetDistanceTo((*p2)->GetName());
		    if (!dist) {
			dist = (*p2)->GetDistanceTo((*p1)->GetName());
			if (!dist) {
			    distance = (*p1)->CalculateDistance(*p2->get());
			}
			else {
			    distance = *dist; 
			}
		    }
		    else {
			distance = *dist;
		    }
		    //cout << distance << endl;
		    curvedLength_ += distance;
		    p1 = p2;
		    p2 = next(p2);
		}
	    }
	}
    }
    return curvedLength_;
}

