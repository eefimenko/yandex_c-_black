#pragma once

#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "json.h"
#include "parameters.h"
#include "stop.h"
#include "transport_manager.h"

struct Request;
using RequestHolder = std::unique_ptr<Request>;

struct Request {
    enum class Type {
	ADD_ROUTE,
	    ADD_STOP,
	    READ_ROUTE,
	    READ_STOP,
	    SET_PARAM,
	    SET_RENDER,
	    CALC_PATH,
	    READ_MAP
    };
  
    Request(Type type) : type(type) {}
    static RequestHolder Create(Type type);
    virtual void ParseFrom(std::string_view input) = 0;
    virtual void ParseFromJSON(const Json::Node& node) = 0;
    virtual ~Request() = default;

    const Type type;
    std::string name;
};

template <typename ResultType>
struct ReadRequest : Request {
    using Request::Request;
    virtual ResultType Process(const TransportManager& manager) const = 0;
    virtual Json::Node ProcessJSON(const TransportManager& manager) const = 0;
};

struct ModifyRequest : Request {
    using Request::Request;
    virtual void Process(TransportManager& manager) const = 0;
    virtual void ProcessJSON(TransportManager& manager) const = 0;
};

struct ReadRouteRequest : ReadRequest<std::string> {
    ReadRouteRequest() : ReadRequest(Type::READ_ROUTE) {}
    void ParseFrom(std::string_view input) override;
    void ParseFromJSON(const Json::Node& node) override;  
    std::string Process(const TransportManager& manager) const override;
    Json::Node ProcessJSON(const TransportManager& manager) const override;
    int id;
};

struct ReadStopRequest : ReadRequest<std::string> {
    ReadStopRequest() : ReadRequest(Type::READ_STOP) {}
    void ParseFrom(std::string_view input) override;
    void ParseFromJSON(const Json::Node& node) override; 
    std::string Process(const TransportManager& manager) const override;
    Json::Node ProcessJSON(const TransportManager& manager) const override;
    
    int id;
};

struct ReadMapRequest : ReadRequest<std::string> {
    ReadMapRequest() : ReadRequest(Type::READ_MAP) {}
    void ParseFrom(std::string_view input) override;
    void ParseFromJSON(const Json::Node& node) override; 
    std::string Process(const TransportManager& manager) const override;
    Json::Node ProcessJSON(const TransportManager& manager) const override;
    
    int id;
};

struct CalcPathRequest : ReadRequest<std::string> {
    CalcPathRequest() : ReadRequest(Type::CALC_PATH) {}
    void ParseFrom(std::string_view input) override;
    void ParseFromJSON(const Json::Node& node) override; 
    std::string Process(const TransportManager& manager) const override;
    Json::Node ProcessJSON(const TransportManager& manager) const override;
    
    int id;
    std::string from;
    std::string to;
};

struct AddStopRequest : ModifyRequest {
    AddStopRequest() : ModifyRequest(Type::ADD_STOP) {}
    void ParseFrom(std::string_view input) override;
    void ParseFromJSON(const Json::Node& node) override;
    void Process(TransportManager& manager) const override;
    void ProcessJSON(TransportManager& manager) const override;

    double lat;
    double lon;
    std::map<std::string, int> distances;
};

struct AddRouteRequest : ModifyRequest {
    AddRouteRequest() : ModifyRequest(Type::ADD_ROUTE) {}
    void ParseFrom(std::string_view input) override;
    void ParseFromJSON(const Json::Node& node) override;
    void Process(TransportManager& manager) const override;
    void ProcessJSON(TransportManager& manager) const override;
    void ParseStops(std::string_view input, char delimiter);

    std::vector<std::string> stops;
    bool isCircular;
};

struct SetParametersRequest : ModifyRequest {
    SetParametersRequest() : ModifyRequest(Type::SET_PARAM) {}
    void ParseFrom(std::string_view input) override;
    void ParseFromJSON(const Json::Node& node) override;
    void Process(TransportManager& manager) const override;
    void ProcessJSON(TransportManager& manager) const override;

    int wait_time;
    double velocity;
};

struct SetRenderRequest : ModifyRequest {
    SetRenderRequest() : ModifyRequest(Type::SET_RENDER) {}
    void ParseFrom(std::string_view input) override;
    void ParseFromJSON(const Json::Node& node) override;
    void Process(TransportManager& manager) const override;
    void ProcessJSON(TransportManager& manager) const override;

    RenderParameters render;
};

std::vector<RequestHolder> ReadRequests(
    std::istream& in_stream = std::cin);

std::vector<RequestHolder> ReadRequestsJSON(
    std::istream& in_stream = std::cin);

std::vector<std::string> ProcessRequests(
    TransportManager& manager,
    const std::vector<RequestHolder>& requests);

std::vector<Json::Node> ProcessRequestsJSON(
    TransportManager& manager,
    const std::vector<RequestHolder>& requests);

void PrintResponses(const std::vector<std::string>& responses,
		    std::ostream& stream = std::cout);

void PrintResponsesJSON(const std::vector<Json::Node>& responses,
		    std::ostream& stream = std::cout);
