#include <iostream>
#include <string>

#include "stop.h"

using namespace std;

void Stop::SetProperties(double latitude,
			 double longitude,
			 const map<string, int>& distances) {
    latitude_ = latitude * 3.1415926535/180.;
    longitude_ = longitude * 3.1415926535/180.;
    distances_ = move(distances);
    areCoordinatesSet_ = true;
};

void Stop::CheckAndMarkIfPivot() {
    /* empty stop or interchange */
    if (routes_.size() == 0 || routes_.size() > 1) {
	isPivot_ = true;
	x_ = longitude_;
	y_ = latitude_;
    }
};

bool Stop::isSet() const {
    return areCoordinatesSet_;
}
    
double Stop::CalculateDistance(const Stop& other) const {
    return acos(sin(latitude_) * sin(other.latitude_) +
		cos(latitude_) * cos(other.latitude_) *
		cos(fabs(longitude_ - other.longitude_))) * 6371000;
}

const set<string>& Stop::GetRoutes() const {
    return routes_;
};

const string& Stop::GetName() const {
    return name_;
};

size_t Stop::GetId() const {
    return id_;
};

void Stop::AddRoute(const std::string& route_name) {
    routes_.insert(route_name);
};

const map<string, int>& Stop::GetDistances() const {
    return distances_;
};

optional<double> Stop::GetDistanceTo(const string& name) {
    auto it = distances_.find(name);
    
    if (it == distances_.end()) {
	return nullopt;
    }
    else {
	return it->second;
    }
};

void Stop::Print() const {
    cout << "Name: " << name_
	 << " set: " << areCoordinatesSet_;
    if (areCoordinatesSet_) {
	cout << " lat: " << latitude_
	     << " lon: " << longitude_;
    }
    cout << endl;
}
