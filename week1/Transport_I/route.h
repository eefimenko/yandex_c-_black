#pragma once

#include <memory>
#include <string>
#include <vector>

#include "stop.h"

class Route {
public:
    explicit Route(const std::string& name, bool isCirc)
	: name_(name)
	, length_(-1.)
	, curvedLength_(-1.)
	, isCircular_(isCirc)
	, n_stops_(-1)
	, n_unique_() {};
    
    Route(const Route& other) = default;
    
    void AddStop(std::shared_ptr<Stop> stop);     
    double GetLength() const;
    const std::string& GetName() const;
    double CalculateLength();
    int CalculateCurvedLength();
    int GetNumberOfStops() const;
    int GetNumberOfUniqueStops() const;
    bool IsCircular() const;
    const std::vector<std::shared_ptr<Stop>>& GetStops() const;
    void Print() const;
    
private:
    std::string name_;
    double length_;
    double curvedLength_;
    std::vector<std::shared_ptr<Stop>> stops_;
    bool isCircular_;
    int n_stops_;
    int n_unique_;
};
