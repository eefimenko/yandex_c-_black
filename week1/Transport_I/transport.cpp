#include <iostream>
#include <vector>

#include "request.h"

using namespace std;

int main() {
    TransportManager manager;
    
    const auto requests = ReadRequestsJSON();
    const auto responses = ProcessRequestsJSON(manager, requests);
    PrintResponsesJSON(responses);
       
    return 0;
}
