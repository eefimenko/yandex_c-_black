#pragma once

#include <vector>

#include "svg.h"

struct RenderParameters {
    double width;
    double height;
    double padding;
    double stop_radius;
    double line_width;
    int stop_label_font_size;
    Svg::Point stop_label_offset;
    Svg::Color underlayer_color;
    double underlayer_width;
    std::vector<Svg::Color> color_palette;
    double zoom_coeff = 0.;
    double min_lon = 0.;
    double max_lat = 0.;
};

