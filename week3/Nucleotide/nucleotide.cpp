#include <cstddef>
#include <cstdint>
#include <unordered_map>

struct Nucleotide {
  char Symbol;
  size_t Position;
  int ChromosomeNum;
  int GeneNum;
  bool IsMarked;
  char ServiceInfo;
};


struct CompactNucleotide {
    uint32_t Position;
    uint32_t Symbol:2;
    uint32_t ChromosomeNum:6;
    uint32_t GeneNum:15;
    uint32_t IsMarked:1;
    uint32_t ServiceInfo:8;
    };
//#include "nucleotide.h"
const std::unordered_map<char, char> SymbolToIdx = {{'A', 0},{'T', 1},{'C', 2}, {'G', 3}};
const std::unordered_map<char, char> IdxToSymbol = {{0, 'A'},{1, 'T'},{2, 'C'}, {3, 'G'}};

bool operator == (const Nucleotide& lhs, const Nucleotide& rhs) {
  return (lhs.Symbol == rhs.Symbol)
      && (lhs.Position == rhs.Position)
      && (lhs.ChromosomeNum == rhs.ChromosomeNum)
      && (lhs.GeneNum == rhs.GeneNum)
      && (lhs.IsMarked == rhs.IsMarked)
      && (lhs.ServiceInfo == rhs.ServiceInfo);
}


CompactNucleotide Compress(const Nucleotide& n) {
    CompactNucleotide result;
    result.Position = n.Position;
    result.Symbol = SymbolToIdx.at(n.Symbol);
    result.ChromosomeNum = n.ChromosomeNum;
    result.GeneNum = n.GeneNum;
    result.IsMarked = n.IsMarked;
    result.ServiceInfo = n.ServiceInfo;
    return result;
};


Nucleotide Decompress(const CompactNucleotide& cn) {
    Nucleotide result;
    result.Position = cn.Position;
    result.Symbol = IdxToSymbol.at(cn.Symbol);
    result.ChromosomeNum = cn.ChromosomeNum;
    result.GeneNum = cn.GeneNum;
    result.IsMarked = cn.IsMarked;
    result.ServiceInfo = cn.ServiceInfo;
    return result;
}
