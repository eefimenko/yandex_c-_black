{"base_requests": [{"latitude": 39.2324
, "longitude": 39.0129
, "name": "agIF"
, "road_distances": {"Z422uH": 907223
}
, "type": "Stop"
}
, {"is_roundtrip": true
, "name": "gw7Q"
, "stops": ["f77"
, "AVivHu1WNWZM1VvTye"
, "DdlDe8chaPdQIrCe"
, "TMJ6Ut0O0k8tk"
, "CPcM0XK7XNzC7"
, "ysNQlFHxW7S7PO"
, "UAm21HbrJbh0OeQDlTqKLRhg"
, "wIT56Gs9GZ2C"
, "dK5ymJu4cTLY2RFhJv9RDPUjJ"
, "T3FVIomqXR"
, "fryanjqLfToRrQWNSOZbfgToD"
, "dK5ymJu4cTLY2RFhJv9RDPUjJ"
, "qDfz4A0E4XpsymIiy3"
, "EFI"
, "FvILRxS"
, "dK5ymJu4cTLY2RFhJv9RDPUjJ"
, "l"
, "618dx6HeatUA0W8izF"
, "b1GIo"
, "1uETRfIYqi5A"
, "AVivHu1WNWZM1VvTye"
, "Z422uH"
, "UAm21HbrJbh0OeQDlTqKLRhg"
, "lmvywwXnSd1aDatOkIx"
, "eBaX7"
, "QkLF"
, "xAFFZnfBv8L0ja"
, "f77"
, "BgGabLVh1xlzPfU9OlTq5mQ6"
, "lmvywwXnSd1aDatOkIx"
, "3sfkAe"
, "4M3bp"
, "CPcM0XK7XNzC7"
, "Gza6t"
, "T3FVIomqXR"
, "l"
, "UAm21HbrJbh0OeQDlTqKLRhg"
, "3mH4Cl"
, "M izkLH"
, "tWuOh3y0f98adADHNxA1wD"
, "3sfkAe"
, "f77"
]
, "type": "Bus"
}
, {"latitude": 39.461
, "longitude": 35.9479
, "name": "bW7xundhpj9ZQQwKI"
, "road_distances": {"AOLY": 773170
, "Y y7OfoVH": 99796
, "Z422uH": 866952
, "mfmMucHVNtjzzu": 852369
}
, "type": "Stop"
}
, {"latitude": 43.3553
, "longitude": 39.917
, "name": "zKg5r5Wr"
, "road_distances": {"Q gTXT6VFScFDpEOJulKFzm": 740347
, "jagWL786Ig": 837116
, "mfmMucHVNtjzzu": 365099
, "nkHOiKz": 531153
, "wIT56Gs9GZ2C": 535121
}
, "type": "Stop"
}
, {"latitude": 45.9822
, "longitude": 37.0585
, "name": "RkWbmgsAJgjF"
, "road_distances": {"3C": 566285
}
, "type": "Stop"
}
, {"latitude": 42.7884
, "longitude": 34.9817
, "name": "AVivHu1WNWZM1VvTye"
, "road_distances": {"6o7CBwM6o3h": 958170
, "DdlDe8chaPdQIrCe": 672059
, "FvILRxS": 803619
, "I5ZTokkEc P7xBL6uQ": 780479
, "Yy2poEi ZhVl4Q": 805900
, "Z422uH": 585358
, "bW7xundhpj9ZQQwKI": 387282
, "l": 616074
, "q9tN1jWPOSxrzOh0A BQGDgsr": 986980
}
, "type": "Stop"
}
, {"latitude": 44.2255
, "longitude": 35.0613
, "name": "Zx DJY8mQJ3lPCH6VIcvyNKp"
, "road_distances": {"FwGioFJb6": 856148
, "H0MtIPPgtTQXk": 881476
, "KGM7QV66WCLcHly73K0Wdpm": 908564
, "hGZh48EWJo": 565580
, "mjnKY WYtH": 938676
}
, "type": "Stop"
}
, {"latitude": 45.1138
, "longitude": 38.4188
, "name": "py5PSu3og3c"
, "road_distances": {"BgGabLVh1xlzPfU9OlTq5mQ6": 345556
, "FvILRxS": 938308
, "dK5ymJu4cTLY2RFhJv9RDPUjJ": 226620
}
, "type": "Stop"
}
, {"latitude": 38.7658
, "longitude": 35.8353
, "name": "tWuOh3y0f98adADHNxA1wD"
, "road_distances": {"3sfkAe": 994826
, "6o7CBwM6o3h": 771161
, "M izkLH": 870613
, "NHgHvsfJm": 862054
}
, "type": "Stop"
}
, {"is_roundtrip": false
, "name": "6kfOFHHI4UnJa8Bs"
, "stops": ["fryanjqLfToRrQWNSOZbfgToD"
, "A"
, "FvILRxS"
, "vrgyynihx8kWY78HK8wtlvQAq"
, "7XY"
, "vrgyynihx8kWY78HK8wtlvQAq"
, "xkN6KMPmlkUO9iRo"
]
, "type": "Bus"
}
, {"latitude": 44.0663
, "longitude": 34.8944
, "name": "V0QlFchPeQAoGJ1ALb"
, "road_distances": {"6o7CBwM6o3h": 696040
, "cMUVXnNZKkdZTghBTfI": 918725
}
, "type": "Stop"
}
, {"latitude": 39.6007
, "longitude": 38.963
, "name": "3C"
, "road_distances": {"0GrPnRTGXZjMvacQv9Cxi": 985270
, "7XY": 956390
, "AVivHu1WNWZM1VvTye": 960254
, "FwGioFJb6": 214181
, "vrgyynihx8kWY78HK8wtlvQAq": 778582
}
, "type": "Stop"
}
, {"latitude": 43.759
, "longitude": 36.6652
, "name": "DNR4dT9jBWH"
, "road_distances": {"s84SPf": 496683
}
, "type": "Stop"
}
, {"latitude": 40.9736
, "longitude": 35.8968
, "name": "xkN6KMPmlkUO9iRo"
, "road_distances": {"EFI": 793418
, "FvILRxS": 471807
, "NaarJ2lsncsk31NVo": 486654
}
, "type": "Stop"
}
, {"latitude": 39.6148
, "longitude": 38.7617
, "name": "3mH4Cl"
, "road_distances": {"M izkLH": 980769
, "QaOZyLMjM": 953407
, "QkLF": 740765
, "SoyrASj327dtMn44": 917993
, "WW": 835282
, "nkHOiKz": 889642
}
, "type": "Stop"
}
, {"latitude": 42.1101
, "longitude": 38.296
, "name": "gsY5igorr2i0"
, "road_distances": {"mfmMucHVNtjzzu": 879858
}
, "type": "Stop"
}
, {"latitude": 42.6589
, "longitude": 39.6324
, "name": "uBCx3WcQ9DSLqkKR7xch"
, "road_distances": {"1UgDwq": 879510
, "FvILRxS": 857225
, "jagWL786Ig": 410072
}
, "type": "Stop"
}
, {"latitude": 46.6442
, "longitude": 35.173
, "name": "KGM7QV66WCLcHly73K0Wdpm"
, "road_distances": {"1UgDwq": 912252
, "7mHaYV3Kjho6u0hkXF2": 708133
, "9NWiNWz2Ji1Nndx8UCjy4dhFZ": 967846
, "BgGabLVh1xlzPfU9OlTq5mQ6": 387616
, "hVn4": 683286
, "vrgyynihx8kWY78HK8wtlvQAq": 246204
}
, "type": "Stop"
}
, {"latitude": 46.0098
, "longitude": 36.7231
, "name": "SoyrASj327dtMn44"
, "road_distances": {"1UgDwq": 986315
, "7mHaYV3Kjho6u0hkXF2": 732563
, "DdlDe8chaPdQIrCe": 832201
, "eX1McBUl": 648670
, "jagWL786Ig": 981945
}
, "type": "Stop"
}
, {"latitude": 43.0878
, "longitude": 39.5053
, "name": "UAm21HbrJbh0OeQDlTqKLRhg"
, "road_distances": {"3mH4Cl": 665456
, "lmvywwXnSd1aDatOkIx": 730310
, "wIT56Gs9GZ2C": 924789
, "zKg5r5Wr": 407549
}
, "type": "Stop"
}
, {"latitude": 40.6295
, "longitude": 36.5134
, "name": "jagWL786Ig"
, "road_distances": {"3C": 978083
, "H0MtIPPgtTQXk": 996453
, "KGM7QV66WCLcHly73K0Wdpm": 723241
, "L7HWINS2DN9CgguoxHXTNy": 552029
, "QaOZyLMjM": 748771
, "Zx DJY8mQJ3lPCH6VIcvyNKp": 751627
}
, "type": "Stop"
}
, {"latitude": 46.4417
, "longitude": 37.7562
, "name": "cMUVXnNZKkdZTghBTfI"
, "road_distances": {"RljovIEVSLiXv6xUgjh5mNQ": 538690
, "Y y7OfoVH": 793271
, "vCzjHIXlHCWrgIC7Mxnuyaw": 325499
}
, "type": "Stop"
}
, {"latitude": 43.7436
, "longitude": 39.1427
, "name": "Z422uH"
, "road_distances": {"1UgDwq": 52166
, "3sfkAe": 810016
, "7pE7c2Sp1KHgwJV6ft": 424896
, "AOLY": 874470
, "UAm21HbrJbh0OeQDlTqKLRhg": 982432
}
, "type": "Stop"
}
, {"latitude": 46.5746
, "longitude": 34.6752
, "name": "7XY"
, "road_distances": {"3mH4Cl": 994337
, "AVivHu1WNWZM1VvTye": 469407
, "RljovIEVSLiXv6xUgjh5mNQ": 600762
, "WG3cJnooavSipJA2hEYLHmY": 793550
, "lmvywwXnSd1aDatOkIx": 350698
, "vrgyynihx8kWY78HK8wtlvQAq": 467937
}
, "type": "Stop"
}
, {"latitude": 40.4799
, "longitude": 36.4284
, "name": "4M3bp"
, "road_distances": {"CPcM0XK7XNzC7": 861817
, "dK5ymJu4cTLY2RFhJv9RDPUjJ": 681072
, "jagWL786Ig": 958486
}
, "type": "Stop"
}
, {"latitude": 41.836
, "longitude": 39.8952
, "name": "mfmMucHVNtjzzu"
, "road_distances": {"EFI": 518094
, "vCzjHIXlHCWrgIC7Mxnuyaw": 851876
, "xkN6KMPmlkUO9iRo": 934829
}
, "type": "Stop"
}
, {"latitude": 45.7547
, "longitude": 38.0726
, "name": "FOH38T"
, "road_distances": {"3mH4Cl": 222058
, "HV7eyEYe7LoL52vx": 232286
, "Z422uH": 474351
}
, "type": "Stop"
}
, {"latitude": 41.8849
, "longitude": 38.9051
, "name": "Yy2poEi ZhVl4Q"
, "road_distances": {"JgROWmVNfSRCjDXa8": 660941
, "SoyrASj327dtMn44": 705316
, "Z422uH": 546952
, "l": 134315
}
, "type": "Stop"
}
, {"is_roundtrip": false
, "name": "M3W4R4FDhHmlHPi9ETk4K5"
, "stops": ["j50AIYNLN1ZStSx v8VE7"
, "1uETRfIYqi5A"
, "CPcM0XK7XNzC7"
, "NaarJ2lsncsk31NVo"
, "I5ZTokkEc P7xBL6uQ"
, "f77"
, "lmvywwXnSd1aDatOkIx"
, "SoyrASj327dtMn44"
, "7mHaYV3Kjho6u0hkXF2"
, "9NWiNWz2Ji1Nndx8UCjy4dhFZ": 967846
, "BgGabLVh1xlzPfU9OlTq5mQ6": 387616
, "hVn4": 683286
, "vrgyynihx8kWY78HK8wtlvQAq": 246204
}
, "type": "Stop"
}
, {"latitude": 46.0098
, "longitude": 36.7231
, "name": "SoyrASj327dtMn44"
, "road_distances": {"1UgDwq": 986315
, "7mHaYV3Kjho6u0hkXF2": 732563
, "DdlDe8chaPdQIrCe": 832201
, "eX1McBUl": 648670
, "jagWL786Ig": 981945
}
, "type": "Stop"
}
, {"latitude": 43.0878
, "longitude": 39.5053
, "name": "UAm21HbrJbh0OeQDlTqKLRhg"
, "road_distances": {"3mH4Cl": 665456
, "lmvywwXnSd1aDatOkIx": 730310
, "wIT56Gs9GZ2C": 924789
, "zKg5r5Wr": 407549
}
, "type": "Stop"
}
, {"latitude": 40.6295
, "longitude": 36.5134
, "name": "jagWL786Ig"
, "road_distances": {"3C": 978083
, "H0MtIPPgtTQXk": 996453
, "KGM7QV66WCLcHly73K0Wdpm": 723241
, "L7HWINS2DN9CgguoxHXTNy": 552029
, "QaOZyLMjM": 748771
, "Zx DJY8mQJ3lPCH6VIcvyNKp": 751627
}
, "type": "Stop"
}
, {"latitude": 46.4417
, "longitude": 37.7562
, "name": "cMUVXnNZKkdZTghBTfI"
, "road_distances": {"RljovIEVSLiXv6xUgjh5mNQ": 538690
, "Y y7OfoVH": 793271
, "vCzjHIXlHCWrgIC7Mxnuyaw": 325499
}
, "type": "Stop"
}
, {"latitude": 43.7436
, "longitude": 39.1427
, "name": "Z422uH"
, "road_distances": {"1UgDwq": 52166
, "3sfkAe": 810016
, "7pE7c2Sp1KHgwJV6ft": 424896
, "AOLY": 874470
, "UAm21HbrJbh0OeQDlTqKLRhg": 982432
}
, "type": "Stop"
}
, {"latitude": 46.5746
, "longitude": 34.6752
, "name": "7XY"
, "road_distances": {"3mH4Cl": 994337
, "AVivHu1WNWZM1VvTye": 469407
, "RljovIEVSLiXv6xUgjh5mNQ": 600762
, "WG3cJnooavSipJA2hEYLHmY": 793550
, "lmvywwXnSd1aDatOkIx": 350698
, "vrgyynihx8kWY78HK8wtlvQAq": 467937
}
, "type": "Stop"
}
, {"latitude": 40.4799
, "longitude": 36.4284
, "name": "4M3bp"
, "road_distances": {"CPcM0XK7XNzC7": 861817
, "dK5ymJu4cTLY2RFhJv9RDPUjJ": 681072
, "jagWL786Ig": 958486
}
, "type": "Stop"
}
, {"latitude": 41.836
, "longitude": 39.8952
, "name": "mfmMucHVNtjzzu"
, "road_distances": {"EFI": 518094
, "vCzjHIXlHCWrgIC7Mxnuyaw": 851876
, "xkN6KMPmlkUO9iRo": 934829
}
, "type": "Stop"
}
, {"latitude": 45.7547
, "longitude": 38.0726
, "name": "FOH38T"
, "road_distances": {"3mH4Cl": 222058
, "HV7eyEYe7LoL52vx": 232286
, "Z422uH": 474351
}
, "type": "Stop"
}
, {"latitude": 41.8849
, "longitude": 38.9051
, "name": "Yy2poEi ZhVl4Q"
, "road_distances": {"JgROWmVNfSRCjDXa8": 660941
, "SoyrASj327dtMn44": 705316
, "Z422uH": 546952
, "l": 134315
}
, "type": "Stop"
}
, {"is_roundtrip": false
, "name": "M3W4R4FDhHmlHPi9ETk4K5"
, "stops": ["j50AIYNLN1ZStSx v8VE7"
, "1uETRfIYqi5A"
, "CPcM0XK7XNzC7"
, "NaarJ2lsncsk31NVo"
, "I5ZTokkEc P7xBL6uQ"
, "f77"
, "lmvywwXnSd1aDatOkIx"
, "SoyrASj327dtMn44"
, "7mHaYV3Kjho6u0hkXF2"
, "j50AIYNLN1ZStSx v8VE7"
, "BgGabLVh1xlzPfU9OlTq5mQ6"
, "AVivHu1WNWZM1VvTye"
, "Yy2poEi ZhVl4Q"
, "JgROWmVNfSRCjDXa8"
, "T3FVIomqXR"
, "f77"
, "V0QlFchPeQAoGJ1ALb"
, "cMUVXnNZKkdZTghBTfI"
, "Y y7OfoVH"
, "j50AIYNLN1ZStSx v8VE7"
, "BgGabLVh1xlzPfU9OlTq5mQ6"
, "I5ZTokkEc P7xBL6uQ"
, "QkLF"
]
, "type": "Bus"
}
, {"latitude": 46.0517
, "longitude": 37.9378
, "name": "vrgyynihx8kWY78HK8wtlvQAq"
, "road_distances": {"3C": 507605
, "618dx6HeatUA0W8izF": 981346
, "7XY": 320834
, "NaarJ2lsncsk31NVo": 577737
, "mfmMucHVNtjzzu": 562593
, "xkN6KMPmlkUO9iRo": 661570
}
, "type": "Stop"
}
, {"latitude": 44.8271
, "longitude": 35.8789
, "name": "dRTs3iHkCKGY5mw9dfxW1Re3"
, "road_distances": {"7XY": 622217
, "FOH38T": 464555
}
, "type": "Stop"
}
, {"latitude": 43.4072
, "longitude": 39.654
, "name": "PhIVsVvwyk4jP"
, "road_distances": {"hVn4": 554669
}
, "type": "Stop"
}
, {"latitude": 39.9218
, "longitude": 34.7046
, "name": "DdlDe8chaPdQIrCe"
, "road_distances": {"EFI": 700772
, "TMJ6Ut0O0k8tk": 626773
, "hVn4": 866749
, "tWuOh3y0f98adADHNxA1wD": 563847
, "ysNQlFHxW7S7PO": 716101
}
, "type": "Stop"
}
, {"latitude": 38.767
, "longitude": 39.1023
, "name": "wIT56Gs9GZ2C"
, "road_distances": {"Q gTXT6VFScFDpEOJulKFzm": 193136
, "SoyrASj327dtMn44": 870833
, "dK5ymJu4cTLY2RFhJv9RDPUjJ": 337546
}
, "type": "Stop"
}
, {"is_roundtrip": false
, "name": "PGttaWWXHpa bhLV0H"
, "stops": ["QaOZyLMjM"
, "q9tN1jWPOSxrzOh0A BQGDgsr"
, "hGZh48EWJo"
, "Zx DJY8mQJ3lPCH6VIcvyNKp"
, "H0MtIPPgtTQXk"
, "dwHSd6JwtxDsDEY"
, "FOH38T"
, "HV7eyEYe7LoL52vx"
, "Z9qORMH1"
, "I5ZTokkEc P7xBL6uQ"
]
, "type": "Bus"
}
, {"latitude": 45.67
, "longitude": 39.8465
, "name": "PdF1h5BlLfgyx5Nd"
, "road_distances": {"7pE7c2Sp1KHgwJV6ft": 885631
, "b1GIo": 95961
, "zKg5r5Wr": 837123
}
, "type": "Stop"
}
, {"latitude": 39.8444
, "longitude": 37.2393
, "name": "f77"
, "road_distances": {"AVivHu1WNWZM1VvTye": 736760
, "BgGabLVh1xlzPfU9OlTq5mQ6": 673592
, "CPcM0XK7XNzC7": 909479
, "V0QlFchPeQAoGJ1ALb": 886241
, "Y y7OfoVH": 451511
, "lmvywwXnSd1aDatOkIx": 619149
}
, "type": "Stop"
}
, {"latitude": 44.5444
, "longitude": 37.9229
, "name": "eBaX7"
, "road_distances": {"618dx6HeatUA0W8izF": 250924
, "QkLF": 477617
}
, "type": "Stop"
}
, {"latitude": 46.367
, "longitude": 37.4677
, "name": "q9tN1jWPOSxrzOh0A BQGDgsr"
, "road_distances": {"Z9qORMH1": 904371
, "hGZh48EWJo": 605503
, "lmvywwXnSd1aDatOkIx": 269055
, "vrgyynihx8kWY78HK8wtlvQAq": 516329
}
, "type": "Stop"
}
, {"latitude": 40.4285
, "longitude": 36.773
, "name": "j50AIYNLN1ZStSx v8VE7"
, "road_distances": {"1uETRfIYqi5A": 934101
, "A": 497901
, "BgGabLVh1xlzPfU9OlTq5mQ6": 625331
, "PhIVsVvwyk4jP": 776206
, "ysNQlFHxW7S7PO": 584148
}
, "type": "Stop"
}
, {"latitude": 44.4713
, "longitude": 34.9902
, "name": "NaarJ2lsncsk31NVo"
, "road_distances": {"I5ZTokkEc P7xBL6uQ": 366838
, "QaOZyLMjM": 497580
, "T3FVIomqXR": 256674
, "nkHOiKz": 311574
}
, "type": "Stop"
}
, {"latitude": 44.0196
, "longitude": 37.1117
, "name": "L7HWINS2DN9CgguoxHXTNy"
, "road_distances": {"cmWx z8ULI0eV": 391400
, "fryanjqLfToRrQWNSOZbfgToD": 35004
}
, "type": "Stop"
}
, {"latitude": 41.3674
, "longitude": 38.0537
, "name": "hGZh48EWJo"
, "road_distances": {"4M3bp": 332084
, "Zx DJY8mQJ3lPCH6VIcvyNKp": 759372
, "zKg5r5Wr": 740118
}
, "type": "Stop"
}
, {"latitude": 44.5882
, "longitude": 36.013
, "name": "WW"
, "road_distances": {"BgGabLVh1xlzPfU9OlTq5mQ6": 70165
, "EFI": 952093
, "RljovIEVSLiXv6xUgjh5mNQ": 282214
, "Z422uH": 319746
}
, "type": "Stop"
}
, {"latitude": 38.6099
, "longitude": 38.4333
, "name": "AJVD6A92m1PhA0"
, "road_distances": {"4GR76tZIxHHT": 931224
, "AOLY": 829188
, "AVivHu1WNWZM1VvTye": 659675
, "DdlDe8chaPdQIrCe": 535492
, "M izkLH": 963091
, "PhIVsVvwyk4jP": 514138
, "RljovIEVSLiXv6xUgjh5mNQ": 911717
, "WW": 836111
, "kuVkSQ0Z0 3": 851519
, "rHy ZZfbVAI": 863360
}
, "type": "Stop"
}
, {"latitude": 46.2652
, "longitude": 35.8908
, "name": "xAFFZnfBv8L0ja"
, "road_distances": {"Yy2poEi ZhVl4Q": 619752
, "f77": 903383
}
, "type": "Stop"
}
, {"latitude": 42.1805
, "longitude": 34.8056
, "name": "TMJ6Ut0O0k8tk"
, "road_distances": {"CPcM0XK7XNzC7": 996247
, "xkN6KMPmlkUO9iRo": 604361
}
, "type": "Stop"
}
, {"latitude": 39.6051
, "longitude": 36.4445
, "name": "FvILRxS"
, "road_distances": {"A": 744690
, "NHgHvsfJm": 667138
, "WW": 304011
, "dK5ymJu4cTLY2RFhJv9RDPUjJ": 786102
, "vrgyynihx8kWY78HK8wtlvQAq": 884485
}
, "type": "Stop"
}
, {"latitude": 41.3467
, "longitude": 37.2842
, "name": "vCzjHIXlHCWrgIC7Mxnuyaw"
, "road_distances": {"FvILRxS": 537835
}
, "type": "Stop"
}
, {"is_roundtrip": false
, "name": "y"
, "stops": ["4GR76tZIxHHT"
, "Yy2poEi ZhVl4Q"
, "SoyrASj327dtMn44"
, "1UgDwq"
, "AJVD6A92m1PhA0"
, "M izkLH"
, "1uETRfIYqi5A"
, "py5PSu3og3c"
, "FvILRxS"
, "A"
, "AJVD6A92m1PhA0"
, "WW"
, "EFI"
, "dK5ymJu4cTLY2RFhJv9RDPUjJ"
, "AVivHu1WNWZM1VvTye"
, "l"
, "BgGabLVh1xlzPfU9OlTq5mQ6"
, "PdF1h5BlLfgyx5Nd"
, "7pE7c2Sp1KHgwJV6ft"
, "vCzjHIXlHCWrgIC7Mxnuyaw"
]
, "type": "Bus"
}
, {"latitude": 38.5418
, "longitude": 34.9721
, "name": "618dx6HeatUA0W8izF"
, "road_distances": {"b1GIo": 522029
, "j50AIYNLN1ZStSx v8VE7": 458389
, "uBCx3WcQ9DSLqkKR7xch": 781897
}
, "type": "Stop"
}
, {"latitude": 41.1594
, "longitude": 35.1361
, "name": "l"
, "road_distances": {"618dx6HeatUA0W8izF": 758638
, "BgGabLVh1xlzPfU9OlTq5mQ6": 950555
, "NHgHvsfJm": 327545
, "UAm21HbrJbh0OeQDlTqKLRhg": 785919
, "hVn4": 892841
, "nkHOiKz": 540919
}
, "type": "Stop"
}
, {"latitude": 45.6565
, "longitude": 37.6664
, "name": "BgGabLVh1xlzPfU9OlTq5mQ6"
, "road_distances": {"AOLY": 610187
, "AVivHu1WNWZM1VvTye": 795865
, "I5ZTokkEc P7xBL6uQ": 408570
, "PdF1h5BlLfgyx5Nd": 448938
, "kuVkSQ0Z0 3": 604249
, "lmvywwXnSd1aDatOkIx": 854839
}
, "type": "Stop"
}
, {"latitude": 39.7058
, "longitude": 36.3679
, "name": "Y y7OfoVH"
, "road_distances": {"dMmGPZc": 481789
, "j50AIYNLN1ZStSx v8VE7": 897545
, "lmvywwXnSd1aDatOkIx": 59236
, "ysNQlFHxW7S7PO": 923631
}
, "type": "Stop"
}
, {"is_roundtrip": true
, "name": "y88h43Gqeqi1"
, "stops": ["tWuOh3y0f98adADHNxA1wD"
, "NHgHvsfJm"
, "4M3bp"
, "dK5ymJu4cTLY2RFhJv9RDPUjJ"
, "Rb63"
, "M izkLH"
, "mjnKY WYtH"
, "dMmGPZc"
, "3sfkAe"
, "AJVD6A92m1PhA0"
, "kuVkSQ0Z0 3"
, "tWuOh3y0f98adADHNxA1wD"
, "M izkLH"
, "AOLY"
, "dRTs3iHkCKGY5mw9dfxW1Re3"
, "7XY"
, "3mH4Cl"
, "WW"
, "Z422uH"
, "7pE7c2Sp1KHgwJV6ft"
, "EFI"
, "AVivHu1WNWZM1VvTye"
, "q9tN1jWPOSxrzOh0A BQGDgsr"
, "vrgyynihx8kWY78HK8wtlvQAq"
, "NaarJ2lsncsk31NVo"
, "nkHOiKz"
, "Q gTXT6VFScFDpEOJulKFzm"
, "1uETRfIYqi5A"
, "HV7eyEYe7LoL52vx"
, "AJVD6A92m1PhA0"
, "rHy ZZfbVAI"
, "Rb63"
, "NHgHvsfJm"
, "7XY"
, "RljovIEVSLiXv6xUgjh5mNQ"
, "3C"
, "7XY"
, "WG3cJnooavSipJA2hEYLHmY"
, "3mH4Cl"
, "QaOZyLMjM"
, "DdlDe8chaPdQIrCe"
, "tWuOh3y0f98adADHNxA1wD"
]
, "type": "Bus"
}
, {"latitude": 43.8762
, "longitude": 38.9555
, "name": "mjnKY WYtH"
, "road_distances": {"Q gTXT6VFScFDpEOJulKFzm": 647595
, "QaOZyLMjM": 345745
, "dMmGPZc": 241449
}
, "type": "Stop"
}
, {"latitude": 41.8981
, "longitude": 35.0935
, "name": "9NWiNWz2Ji1Nndx8UCjy4dhFZ"
, "road_distances": {"1UgDwq": 714070
, "jagWL786Ig": 382958
, "py5PSu3og3c": 550369
}
, "type": "Stop"
}
, {"latitude": 42.7754
, "longitude": 35.5938
, "name": "6o7CBwM6o3h"
, "road_distances": {"NHgHvsfJm": 538766
, "hVn4": 332866
}
, "type": "Stop"
}
, {"latitude": 44.6886
, "longitude": 38.8363
, "name": "M izkLH"
, "road_distances": {"1uETRfIYqi5A": 905448
, "AOLY": 886367
, "j50AIYNLN1ZStSx v8VE7": 687726
, "mjnKY WYtH": 362978
, "tWuOh3y0f98adADHNxA1wD": 810567
}
, "type": "Stop"
}
, {"latitude": 46.3865
, "longitude": 39.3293
, "name": "QkLF"
, "road_distances": {"FOH38T": 569189
, "xAFFZnfBv8L0ja": 622562
}
, "type": "Stop"
}
, {"latitude": 38.9165
, "longitude": 39.7953
, "name": "A"
, "road_distances": {"AJVD6A92m1PhA0": 277497
, "FvILRxS": 582610
, "JgROWmVNfSRCjDXa8": 758275
, "Zx DJY8mQJ3lPCH6VIcvyNKp": 807127
, "eX1McBUl": 807668
, "l": 572303
}
, "type": "Stop"
}
, {"latitude": 38.5441
, "longitude": 37.5698
, "name": "Gg"
, "road_distances": {"3C": 725171
, "EFI": 1972
, "T3FVIomqXR": 547373
}
, "type": "Stop"
}
, {"is_roundtrip": true
, "name": "wAn0HRoK"
, "stops": ["QaOZyLMjM"
, "q9tN1jWPOSxrzOh0A BQGDgsr"
, "hGZh48EWJo"
, "Zx DJY8mQJ3lPCH6VIcvyNKp"
, "H0MtIPPgtTQXk"
, "dwHSd6JwtxDsDEY"
, "FOH38T"
, "HV7eyEYe7LoL52vx"
, "Z9qORMH1"
, "I5ZTokkEc P7xBL6uQ"
]
, "type": "Bus"
}
, {"latitude": 45.67
, "longitude": 39.8465
, "name": "PdF1h5BlLfgyx5Nd"
, "road_distances": {"7pE7c2Sp1KHgwJV6ft": 885631
, "b1GIo": 95961
, "zKg5r5Wr": 837123
}
, "type": "Stop"
}
, {"latitude": 39.8444
, "longitude": 37.2393
, "name": "f77"
, "road_distances": {"AVivHu1WNWZM1VvTye": 736760
, "BgGabLVh1xlzPfU9OlTq5mQ6": 673592
, "CPcM0XK7XNzC7": 909479
, "V0QlFchPeQAoGJ1ALb": 886241
, "Y y7OfoVH": 451511
, "lmvywwXnSd1aDatOkIx": 619149
}
, "type": "Stop"
}
, {"latitude": 44.5444
, "longitude": 37.9229
, "name": "eBaX7"
, "road_distances": {"618dx6HeatUA0W8izF": 250924
, "QkLF": 477617
}
, "type": "Stop"
}
, {"latitude": 46.367
, "longitude": 37.4677
, "name": "q9tN1jWPOSxrzOh0A BQGDgsr"
, "road_distances": {"Z9qORMH1": 904371
, "hGZh48EWJo": 605503
, "lmvywwXnSd1aDatOkIx": 269055
, "vrgyynihx8kWY78HK8wtlvQAq": 516329
}
, "type": "Stop"
}
, {"latitude": 40.4285
, "longitude": 36.773
, "name": "j50AIYNLN1ZStSx v8VE7"
, "road_distances": {"1uETRfIYqi5A": 934101
, "A": 497901
, "BgGabLVh1xlzPfU9OlTq5mQ6": 625331
, "PhIVsVvwyk4jP": 776206
, "ysNQlFHxW7S7PO": 584148
}
, "type": "Stop"
}
, {"latitude": 44.4713
, "longitude": 34.9902
, "name": "NaarJ2lsncsk31NVo"
, "road_distances": {"I5ZTokkEc P7xBL6uQ": 366838
, "QaOZyLMjM": 497580
, "T3FVIomqXR": 256674
, "nkHOiKz": 311574
}
, "type": "Stop"
}
, {"latitude": 44.0196
, "longitude": 37.1117
, "name": "L7HWINS2DN9CgguoxHXTNy"
, "road_distances": {"cmWx z8ULI0eV": 391400
, "fryanjqLfToRrQWNSOZbfgToD": 35004
}
, "type": "Stop"
}
, {"latitude": 41.3674
, "longitude": 38.0537
, "name": "hGZh48EWJo"
, "road_distances": {"4M3bp": 332084
, "Zx DJY8mQJ3lPCH6VIcvyNKp": 759372
, "zKg5r5Wr": 740118
}
, "type": "Stop"
}
, {"latitude": 44.5882
, "longitude": 36.013
, "name": "WW"
, "road_distances": {"BgGabLVh1xlzPfU9OlTq5mQ6": 70165
, "EFI": 952093
, "RljovIEVSLiXv6xUgjh5mNQ": 282214
, "Z422uH": 319746
}
, "type": "Stop"
}
, {"latitude": 38.6099
, "longitude": 38.4333
, "name": "AJVD6A92m1PhA0"
, "road_distances": {"4GR76tZIxHHT": 931224
, "AOLY": 829188
, "AVivHu1WNWZM1VvTye": 659675
, "DdlDe8chaPdQIrCe": 535492
, "M izkLH": 963091
, "PhIVsVvwyk4jP": 514138
, "RljovIEVSLiXv6xUgjh5mNQ": 911717
, "WW": 836111
, "kuVkSQ0Z0 3": 851519
, "rHy ZZfbVAI": 863360
}
, "type": "Stop"
}
, {"latitude": 46.2652
, "longitude": 35.8908
, "name": "xAFFZnfBv8L0ja"
, "road_distances": {"Yy2poEi ZhVl4Q": 619752
, "f77": 903383
}
, "type": "Stop"
}
, {"latitude": 42.1805
, "longitude": 34.8056
, "name": "TMJ6Ut0O0k8tk"
, "road_distances": {"CPcM0XK7XNzC7": 996247
, "xkN6KMPmlkUO9iRo": 604361
}
, "type": "Stop"
}
, {"latitude": 39.6051
, "longitude": 36.4445
, "name": "FvILRxS"
, "road_distances": {"A": 744690
, "NHgHvsfJm": 667138
, "WW": 304011
, "dK5ymJu4cTLY2RFhJv9RDPUjJ": 786102
, "vrgyynihx8kWY78HK8wtlvQAq": 884485
}
, "type": "Stop"
}
, {"latitude": 41.3467
, "longitude": 37.2842
, "name": "vCzjHIXlHCWrgIC7Mxnuyaw"
, "road_distances": {"FvILRxS": 537835
}
, "type": "Stop"
}
, {"is_roundtrip": false
, "name": "y"
, "stops": ["4GR76tZIxHHT"
, "Yy2poEi ZhVl4Q"
, "SoyrASj327dtMn44"
, "1UgDwq"
, "AJVD6A92m1PhA0"
, "M izkLH"
, "1uETRfIYqi5A"
, "py5PSu3og3c"
, "FvILRxS"
, "A"
, "AJVD6A92m1PhA0"
, "WW"
, "EFI"
, "dK5ymJu4cTLY2RFhJv9RDPUjJ"
, "AVivHu1WNWZM1VvTye"
, "l"
, "BgGabLVh1xlzPfU9OlTq5mQ6"
, "PdF1h5BlLfgyx5Nd"
, "7pE7c2Sp1KHgwJV6ft"
, "vCzjHIXlHCWrgIC7Mxnuyaw"
]
, "type": "Bus"
}
, {"latitude": 38.5418
, "longitude": 34.9721
, "name": "618dx6HeatUA0W8izF"
, "road_distances": {"b1GIo": 522029
, "j50AIYNLN1ZStSx v8VE7": 458389
, "uBCx3WcQ9DSLqkKR7xch": 781897
}
, "type": "Stop"
}
, {"latitude": 41.1594
, "longitude": 35.1361
, "name": "l"
, "road_distances": {"618dx6HeatUA0W8izF": 758638
, "BgGabLVh1xlzPfU9OlTq5mQ6": 950555
, "NHgHvsfJm": 327545
, "UAm21HbrJbh0OeQDlTqKLRhg": 785919
, "hVn4": 892841
, "nkHOiKz": 540919
}
, "type": "Stop"
}
, {"latitude": 45.6565
, "longitude": 37.6664
, "name": "BgGabLVh1xlzPfU9OlTq5mQ6"
, "road_distances": {"AOLY": 610187
, "AVivHu1WNWZM1VvTye": 795865
, "I5ZTokkEc P7xBL6uQ": 408570
, "PdF1h5BlLfgyx5Nd": 448938
, "kuVkSQ0Z0 3": 604249
, "lmvywwXnSd1aDatOkIx": 854839
}
, "type": "Stop"
}
, {"latitude": 39.7058
, "longitude": 36.3679
, "name": "Y y7OfoVH"
, "road_distances": {"dMmGPZc": 481789
, "j50AIYNLN1ZStSx v8VE7": 897545
, "lmvywwXnSd1aDatOkIx": 59236
, "ysNQlFHxW7S7PO": 923631
}
, "type": "Stop"
}
, {"is_roundtrip": true
, "name": "y88h43Gqeqi1"
, "stops": ["tWuOh3y0f98adADHNxA1wD"
, "NHgHvsfJm"
, "4M3bp"
, "dK5ymJu4cTLY2RFhJv9RDPUjJ"
, "Rb63"
, "M izkLH"
, "mjnKY WYtH"
, "dMmGPZc"
, "3sfkAe"
, "AJVD6A92m1PhA0"
, "kuVkSQ0Z0 3"
, "tWuOh3y0f98adADHNxA1wD"
, "M izkLH"
, "AOLY"
, "dRTs3iHkCKGY5mw9dfxW1Re3"
, "7XY"
, "3mH4Cl"
, "WW"
, "Z422uH"
, "7pE7c2Sp1KHgwJV6ft"
, "EFI"
, "AVivHu1WNWZM1VvTye"
, "q9tN1jWPOSxrzOh0A BQGDgsr"
, "vrgyynihx8kWY78HK8wtlvQAq"
, "NaarJ2lsncsk31NVo"
, "nkHOiKz"
, "Q gTXT6VFScFDpEOJulKFzm"
, "1uETRfIYqi5A"
, "HV7eyEYe7LoL52vx"
, "AJVD6A92m1PhA0"
, "rHy ZZfbVAI"
, "Rb63"
, "NHgHvsfJm"
, "7XY"
, "RljovIEVSLiXv6xUgjh5mNQ"
, "3C"
, "7XY"
, "WG3cJnooavSipJA2hEYLHmY"
, "3mH4Cl"
, "QaOZyLMjM"
, "DdlDe8chaPdQIrCe"
, "tWuOh3y0f98adADHNxA1wD"
]
, "type": "Bus"
}
, {"latitude": 43.8762
, "longitude": 38.9555
, "name": "mjnKY WYtH"
, "road_distances": {"Q gTXT6VFScFDpEOJulKFzm": 647595
, "QaOZyLMjM": 345745
, "dMmGPZc": 241449
}
, "type": "Stop"
}
, {"latitude": 41.8981
, "longitude": 35.0935
, "name": "9NWiNWz2Ji1Nndx8UCjy4dhFZ"
, "road_distances": {"1UgDwq": 714070
, "jagWL786Ig": 382958
, "py5PSu3og3c": 550369
}
, "type": "Stop"
}
, {"latitude": 42.7754
, "longitude": 35.5938
, "name": "6o7CBwM6o3h"
, "road_distances": {"NHgHvsfJm": 538766
, "hVn4": 332866
}
, "type": "Stop"
}
, {"latitude": 44.6886
, "longitude": 38.8363
, "name": "M izkLH"
, "road_distances": {"1uETRfIYqi5A": 905448
, "AOLY": 886367
, "j50AIYNLN1ZStSx v8VE7": 687726
, "mjnKY WYtH": 362978
, "tWuOh3y0f98adADHNxA1wD": 810567
}
, "type": "Stop"
}
, {"latitude": 46.3865
, "longitude": 39.3293
, "name": "QkLF"
, "road_distances": {"FOH38T": 569189
, "xAFFZnfBv8L0ja": 622562
}
, "type": "Stop"
}
, {"latitude": 38.9165
, "longitude": 39.7953
, "name": "A"
, "road_distances": {"AJVD6A92m1PhA0": 277497
, "FvILRxS": 582610
, "JgROWmVNfSRCjDXa8": 758275
, "Zx DJY8mQJ3lPCH6VIcvyNKp": 807127
, "eX1McBUl": 807668
, "l": 572303
}
, "type": "Stop"
}
, {"latitude": 38.5441
, "longitude": 37.5698
, "name": "Gg"
, "road_distances": {"3C": 725171
, "EFI": 1972
, "T3FVIomqXR": 547373
}
, "type": "Stop"
}
, {"is_roundtrip": true
, "name": "wAn0HRoK"
, "stops": ["QaOZyLMjM"
, "q9tN1jWPOSxrzOh0A BQGDgsr"
, "hGZh48EWJo"
, "Zx DJY8mQJ3lPCH6VIcvyNKp"
, "H0MtIPPgtTQXk"
, "dwHSd6JwtxDsDEY"
, "FOH38T"
, "HV7eyEYe7LoL52vx"
, "Z9qORMH1"
, "I5ZTokkEc P7xBL6uQ"
]
, "type": "Bus"
}
, {"latitude": 45.67
, "longitude": 39.8465
, "name": "PdF1h5BlLfgyx5Nd"
, "road_distances": {"7pE7c2Sp1KHgwJV6ft": 885631
, "b1GIo": 95961
, "zKg5r5Wr": 837123
}
, "type": "Stop"
}
, {"latitude": 39.8444
, "longitude": 37.2393
, "name": "f77"
, "road_distances": {"AVivHu1WNWZM1VvTye": 736760
, "BgGabLVh1xlzPfU9OlTq5mQ6": 673592
, "CPcM0XK7XNzC7": 909479
, "V0QlFchPeQAoGJ1ALb": 886241
, "Y y7OfoVH": 451511
, "lmvywwXnSd1aDatOkIx": 619149
}
, "type": "Stop"
}
, {"latitude": 44.5444
, "longitude": 37.9229
, "name": "eBaX7"
, "road_distances": {"618dx6HeatUA0W8izF": 250924
, "QkLF": 477617
}
, "type": "Stop"
}
, {"latitude": 46.367
, "longitude": 37.4677
, "name": "q9tN1jWPOSxrzOh0A BQGDgsr"
, "road_distances": {"Z9qORMH1": 904371
, "hGZh48EWJo": 605503
, "lmvywwXnSd1aDatOkIx": 269055
, "vrgyynihx8kWY78HK8wtlvQAq": 516329
}
, "type": "Stop"
}
, {"latitude": 40.4285
, "longitude": 36.773
, "name": "j50AIYNLN1ZStSx v8VE7"
, "road_distances": {"1uETRfIYqi5A": 934101
, "A": 497901
, "BgGabLVh1xlzPfU9OlTq5mQ6": 625331
, "PhIVsVvwyk4jP": 776206
, "ysNQlFHxW7S7PO": 584148
}
, "type": "Stop"
}
, {"latitude": 44.4713
, "longitude": 34.9902
, "name": "NaarJ2lsncsk31NVo"
, "road_distances": {"I5ZTokkEc P7xBL6uQ": 366838
, "QaOZyLMjM": 497580
, "T3FVIomqXR": 256674
, "nkHOiKz": 311574
}
, "type": "Stop"
}
, {"latitude": 44.0196
, "longitude": 37.1117
, "name": "L7HWINS2DN9CgguoxHXTNy"
, "road_distances": {"cmWx z8ULI0eV": 391400
, "fryanjqLfToRrQWNSOZbfgToD": 35004
}
, "type": "Stop"
}
, {"latitude": 41.3674
, "longitude": 38.0537
, "name": "hGZh48EWJo"
, "road_distances": {"4M3bp": 332084
, "Zx DJY8mQJ3lPCH6VIcvyNKp": 759372
, "zKg5r5Wr": 740118
}
, "type": "Stop"
}
, {"latitude": 44.5882
, "longitude": 36.013
, "name": "WW"
, "road_distances": {"BgGabLVh1xlzPfU9OlTq5mQ6": 70165
, "EFI": 952093
, "RljovIEVSLiXv6xUgjh5mNQ": 282214
, "Z422uH": 319746
}
, "type": "Stop"
}
, {"latitude": 38.6099
, "longitude": 38.4333
, "name": "AJVD6A92m1PhA0"
, "road_distances": {"4GR76tZIxHHT": 931224
, "AOLY": 829188
, "AVivHu1WNWZM1VvTye": 659675
, "DdlDe8chaPdQIrCe": 535492
, "M izkLH": 963091
, "PhIVsVvwyk4jP": 514138
, "RljovIEVSLiXv6xUgjh5mNQ": 911717
, "WW": 836111
, "kuVkSQ0Z0 3": 851519
, "rHy ZZfbVAI": 863360
}
, "type": "Stop"
}
, {"latitude": 46.2652
, "longitude": 35.8908
, "name": "xAFFZnfBv8L0ja"
, "road_distances": {"Yy2poEi ZhVl4Q": 619752
, "f77": 903383
}
, "type": "Stop"
}
, {"latitude": 42.1805
, "longitude": 34.8056
, "name": "TMJ6Ut0O0k8tk"
, "road_distances": {"CPcM0XK7XNzC7": 996247
, "xkN6KMPmlkUO9iRo": 604361
}
, "type": "Stop"
}
, {"latitude": 39.6051
, "longitude": 36.4445
, "name": "FvILRxS"
, "road_distances": {"A": 744690
, "NHgHvsfJm": 667138
, "WW": 304011
, "dK5ymJu4cTLY2RFhJv9RDPUjJ": 786102
, "vrgyynihx8kWY78HK8wtlvQAq": 884485
}
, "type": "Stop"
}
, {"latitude": 41.3467
, "longitude": 37.2842
, "name": "vCzjHIXlHCWrgIC7Mxnuyaw"
, "road_distances": {"FvILRxS": 537835
}
, "type": "Stop"
}
, {"is_roundtrip": false
, "name": "y"
, "stops": ["4GR76tZIxHHT"
, "Yy2poEi ZhVl4Q"
, "SoyrASj327dtMn44"
, "1UgDwq"
, "AJVD6A92m1PhA0"
, "M izkLH"
, "1uETRfIYqi5A"
, "py5PSu3og3c"
, "FvILRxS"
, "A"
, "AJVD6A92m1PhA0"
, "WW"
, "EFI"
, "dK5ymJu4cTLY2RFhJv9RDPUjJ"
, "AVivHu1WNWZM1VvTye"
, "l"
, "BgGabLVh1xlzPfU9OlTq5mQ6"
, "PdF1h5BlLfgyx5Nd"
, "7pE7c2Sp1KHgwJV6ft"
, "vCzjHIXlHCWrgIC7Mxnuyaw"
]
, "type": "Bus"
}
, {"latitude": 38.5418
, "longitude": 34.9721
, "name": "618dx6HeatUA0W8izF"
, "road_distances": {"b1GIo": 522029
, "j50AIYNLN1ZStSx v8VE7": 458389
, "uBCx3WcQ9DSLqkKR7xch": 781897
}
, "type": "Stop"
}
, {"latitude": 41.1594
, "longitude": 35.1361
, "name": "l"
, "road_distances": {"618dx6HeatUA0W8izF": 758638
, "BgGabLVh1xlzPfU9OlTq5mQ6": 950555
, "NHgHvsfJm": 327545
, "UAm21HbrJbh0OeQDlTqKLRhg": 785919
, "hVn4": 892841
, "nkHOiKz": 540919
}
, "type": "Stop"
}
, {"latitude": 45.6565
, "longitude": 37.6664
, "name": "BgGabLVh1xlzPfU9OlTq5mQ6"
, "road_distances": {"AOLY": 610187
, "AVivHu1WNWZM1VvTye": 795865
, "I5ZTokkEc P7xBL6uQ": 408570
, "PdF1h5BlLfgyx5Nd": 448938
, "kuVkSQ0Z0 3": 604249
, "lmvywwXnSd1aDatOkIx": 854839
}
, "type": "Stop"
}
, {"latitude": 39.7058
, "longitude": 36.3679
, "name": "Y y7OfoVH"
, "road_distances": {"dMmGPZc": 481789
, "j50AIYNLN1ZStSx v8VE7": 897545
, "lmvywwXnSd1aDatOkIx": 59236
, "ysNQlFHxW7S7PO": 923631
}
, "type": "Stop"
}
, {"is_roundtrip": true
, "name": "y88h43Gqeqi1"
, "stops": ["tWuOh3y0f98adADHNxA1wD"
, "NHgHvsfJm"
, "4M3bp"
, "dK5ymJu4cTLY2RFhJv9RDPUjJ"
, "Rb63"
, "M izkLH"
, "mjnKY WYtH"
, "dMmGPZc"
, "3sfkAe"
, "AJVD6A92m1PhA0"
, "kuVkSQ0Z0 3"
, "tWuOh3y0f98adADHNxA1wD"
, "M izkLH"
, "AOLY"
, "dRTs3iHkCKGY5mw9dfxW1Re3"
, "7XY"
, "3mH4Cl"
, "WW"
, "Z422uH"
, "7pE7c2Sp1KHgwJV6ft"
, "EFI"
, "AVivHu1WNWZM1VvTye"
, "q9tN1jWPOSxrzOh0A BQGDgsr"
, "vrgyynihx8kWY78HK8wtlvQAq"
, "NaarJ2lsncsk31NVo"
, "nkHOiKz"
, "Q gTXT6VFScFDpEOJulKFzm"
, "1uETRfIYqi5A"
, "HV7eyEYe7LoL52vx"
, "AJVD6A92m1PhA0"
, "rHy ZZfbVAI"
, "Rb63"
, "NHgHvsfJm"
, "7XY"
, "RljovIEVSLiXv6xUgjh5mNQ"
, "3C"
, "7XY"
, "WG3cJnooavSipJA2hEYLHmY"
, "3mH4Cl"
, "QaOZyLMjM"
, "DdlDe8chaPdQIrCe"
, "tWuOh3y0f98adADHNxA1wD"
]
, "type": "Bus"
}
, {"latitude": 43.8762
, "longitude": 38.9555
, "name": "mjnKY WYtH"
, "road_distances": {"Q gTXT6VFScFDpEOJulKFzm": 647595
, "QaOZyLMjM": 345745
, "dMmGPZc": 241449
}
, "type": "Stop"
}
, {"latitude": 41.8981
, "longitude": 35.0935
, "name": "9NWiNWz2Ji1Nndx8UCjy4dhFZ"
, "road_distances": {"1UgDwq": 714070
, "jagWL786Ig": 382958
, "py5PSu3og3c": 550369
}
, "type": "Stop"
}
, {"latitude": 42.7754
, "longitude": 35.5938
, "name": "6o7CBwM6o3h"
, "road_distances": {"NHgHvsfJm": 538766
, "hVn4": 332866
}
, "type": "Stop"
}
, {"latitude": 44.6886
, "longitude": 38.8363
, "name": "M izkLH"
, "road_distances": {"1uETRfIYqi5A": 905448
, "AOLY": 886367
, "j50AIYNLN1ZStSx v8VE7": 687726
, "mjnKY WYtH": 362978
, "tWuOh3y0f98adADHNxA1wD": 810567
}
, "type": "Stop"
}
, {"latitude": 46.3865
, "longitude": 39.3293
, "name": "QkLF"
, "road_distances": {"FOH38T": 569189
, "xAFFZnfBv8L0ja": 622562
}
, "type": "Stop"
}
, {"latitude": 38.9165
, "longitude": 39.7953
, "name": "A"
, "road_distances": {"AJVD6A92m1PhA0": 277497
, "FvILRxS": 582610
, "JgROWmVNfSRCjDXa8": 758275
, "Zx DJY8mQJ3lPCH6VIcvyNKp": 807127
, "eX1McBUl": 807668
, "l": 572303
}
, "type": "Stop"
}
, {"latitude": 38.5441
, "longitude": 37.5698
, "name": "Gg"
, "road_distances": {"3C": 725171
, "EFI": 1972
, "T3FVIomqXR": 547373
}
, "type": "Stop"
}
, {"is_roundtrip": true
, "name": "wAn0HRoK"
, "stops": ["QaOZyLMjM"