#pragma once

#include <iostream>
#include <memory>
#include <optional>
#include <variant>
#include <vector>

#include "json.h"

namespace Svg {
        
    struct Point {
    	double x;
	double y;
        Point() : x(0), y(0) {};
        Point(double xp, double yp) : x(xp), y(yp) {};
	Point(const Json::Node& node) {
	    auto& point = node.AsArray();
	    x = point[0].AsDouble();
	    y = point[1].AsDouble();
	}
	std::string ToString() const {
	    return std::to_string(x) + "," +
		std::to_string(y);
	}
    };

    struct Rgb {
    	unsigned char red;
	unsigned char green;
	unsigned char blue;
        Rgb() : red(0), green(0), blue(0) {};
        Rgb(unsigned char r,
	    unsigned char g,
	    unsigned char b) : red(r), green(g), blue(b) {};
	std::string ToString() const {
	    return "rgb("
		+ std::to_string(red) + ","
		+ std::to_string(green) + ","
		+ std::to_string(blue) + ")";
	}
    };

    struct Rgba {
    	unsigned char red;
	unsigned char green;
	unsigned char blue;
	double alpha;
	
        Rgba() : red(0), green(0), blue(0), alpha(0.) {};
        Rgba(unsigned char r,
	     unsigned char g,
	     unsigned char b,
	     double a
	    ) : red(r), green(g), blue(b), alpha(a)  {};
	std::string ToString() const {
	    return "rgba("
		+ std::to_string(red) + ","
		+ std::to_string(green) + ","
		+ std::to_string(blue) + ","
		+ std::to_string(alpha) + ")";
	}
    };
       
    class Color {
    public:
        Color() {};
        Color(const std::string& color) : color_(color) {};
        Color(const char* color) : color_(color) {};
        Color(const Rgb& rgb) : color_(rgb) {};
        Color(const Rgba& rgba) : color_(rgba) {};
	Color(const Json::Node& node) {
	    int type = node.index();
    	    if (type == 0)  { // vector<Node>
		auto& cmap = node.AsArray();
		if (cmap.size() == 3) { // Rgb
		    color_ = Rgb(cmap[0].AsInt(),
				 cmap[1].AsInt(),
				 cmap[2].AsInt());
		}
		else if (cmap.size() == 4) { // Rgba
		    color_ = Rgba(cmap[0].AsInt(),
				  cmap[1].AsInt(),
				  cmap[2].AsInt(),
				  cmap[3].AsDouble());
		}
	    }
	    else if (type == 5) {
		color_ = node.AsString();
	    }
	};
	
	std::string ToString() const {
	    return std::visit([this](const auto& value) {return this->RenderColor(value); }, color_);
	}
    private:
	std::string RenderColor(std::monostate) const {
	    return "none";
	}

	std::string RenderColor(const std::string& color) const {
	    return color;
	}

	std::string RenderColor(Rgb rgb) const {
	    return rgb.ToString();
	}

	std::string RenderColor(Rgba rgba) const {
	    return rgba.ToString();
	}
	
	std::variant<std::monostate, std::string, Rgb, Rgba> color_;
    };

    const Color NoneColor = Color();

    class Object {
    public:
        Object() = default;
	Object(const Object& other) = default;
	Object(Object&& other) = default;
	Object& operator=(const Object& other) = default;
	Object& operator=(Object&& other) = default;

	virtual ~Object() = default;
	
	Object& SetFillColor(const Color& color) {
	    fillColor_ = color;
	    return *this;
	};
        Object& SetStrokeColor(const Color& color) {
	    strokeColor_ = color;
	    return *this;
	};
	Object& SetStrokeWidth(double width) {
	    strokeWidth_ = width;
	    return *this;
	};
	Object& SetStrokeLineCap(const std::string& cap) {
	    strokeLineCap_ = cap;
	    return *this;
	};
	Object& SetStrokeLineJoin(const std::string& join) {
	    strokeLineJoin_ = join;
	    return *this;
	};
	
	void RenderCommon(std::ostream& out) const {
	    out << " fill=\"" << fillColor_.ToString() << "\"";
	    out << " stroke=\"" << strokeColor_.ToString() << "\"";
	    out << " stroke-width=\"" << std::to_string(strokeWidth_) <<"\"";
	    if (strokeLineCap_) {
		out << " stroke-linecap=\""
		    << strokeLineCap_.value() << "\"";
	    }
	    if (strokeLineJoin_) {
		out << " stroke-linejoin=\""
		    << strokeLineJoin_.value() << "\"";
	    }
	};
	
	void RenderCommonJSON(std::ostream& out) const {
	    out << R"( fill=\")" << fillColor_.ToString() << R"(\")";
	    out << R"( stroke=\")" << strokeColor_.ToString() << R"(\")";
	    out << R"( stroke-width=\")" << std::to_string(strokeWidth_) <<R"(\")";
	    if (strokeLineCap_) {
		out << R"( stroke-linecap=\")"
		    << strokeLineCap_.value() << R"(\")";
	    }
	    if (strokeLineJoin_) {
		out << R"( stroke-linejoin=\")"
		    << strokeLineJoin_.value() << R"(\")";
	    }
	};
	
	virtual void Render(std::ostream& out) const = 0;
	virtual void RenderJSON(std::ostream& out) const = 0;
    private:
	Color fillColor_ = NoneColor;
	Color strokeColor_ = NoneColor;
	double strokeWidth_ = 1.0;
	std::optional<std::string> strokeLineCap_;
	std::optional<std::string> strokeLineJoin_;
    };

    template <typename Derived>
    class ObjectAdapter : public Object {
    public:
	Derived& SetFillColor(const Color& color) {
	    return static_cast<Derived&>(Object::SetFillColor(std::move(color)));
	};
	
	Derived& SetStrokeColor(const Color& color) {
	    return static_cast<Derived&>(Object::SetStrokeColor(std::move(color)));
	};
	
	Derived& SetStrokeWidth(double width) {
	    return static_cast<Derived&>(Object::SetStrokeWidth(width));
	};
	
	Derived& SetStrokeLineCap(const std::string& cap) {
	    return static_cast<Derived&>(Object::SetStrokeLineCap(std::move(cap)));
	};
	
	Derived& SetStrokeLineJoin(const std::string& join) {
	    return static_cast<Derived&>(Object::SetStrokeLineJoin(std::move(join)));
	};
    };
    
    class Polyline : public ObjectAdapter<Polyline> {
    public:
	Polyline() = default;
	
	Polyline& AddPoint(Point point) {
	    data_.push_back(point);
	    return *this;
	};
	
	void Render(std::ostream& out) const override {
	    out << "<polyline ";
	    Object::RenderCommon(out);
	    out << " points=\"";
	    for (const auto& p : data_) {
		out << p.ToString() << " ";
	    }
	    out << "\"/>";
	};
	
	void RenderJSON(std::ostream& out) const override {
	    out << "<polyline ";
	    Object::RenderCommonJSON(out);
	    out << R"( points=\")";
	    for (const auto& p : data_) {
		out << p.ToString() << " ";
	    }
	    out << R"(\"/>)";
	};
    private:
	std::vector<Point> data_;
    };
    
    class Circle : public ObjectAdapter<Circle> {
    public:
	Circle() = default;
	Circle& SetRadius(double r) {
	    r_ = r;
	    return *this;
	}
	Circle& SetCenter(Point center) {
	    center_ = center;
	    return *this;
	}
	
	void Render(std::ostream& out) const override {
	    out << "<circle ";
	    Object::RenderCommon(out);
	    out << " cx=\"" << std::to_string(center_.x) << "\"";
	    out << " cy=\"" << std::to_string(center_.y) << "\"";
	    out << " r=\"" << std::to_string(r_) << "\"";
	    out << "/>";
	};
	
	void RenderJSON(std::ostream& out) const override {
	    out << "<circle ";
	    Object::RenderCommonJSON(out);
	    out << R"( cx=\")" << std::to_string(center_.x) << R"(\")";
	    out << R"( cy=\")" << std::to_string(center_.y) << R"(\")";
	    out << R"( r=\")" << std::to_string(r_) << R"(\")";
	    out << "/>";
	};
    private:
	Point center_ = {0., 0.};
	double r_ = 1.0;
    };

    class Text : public ObjectAdapter<Text> {
    public:
	Text() = default;
	Text& SetPoint(Point point) {
	    point_ = point;
	    return *this;
	};
	Text& SetOffset(Point offset) {
	    offset_ = offset;
	    return *this;
	};
	Text& SetFontSize(uint32_t fontsize) {
	    fontsize_ = fontsize;
	    return *this;
	};
	Text& SetFontFamily(const std::string& fontFamily) {
	    fontFamily_ = fontFamily;
	    return *this;
	};
	Text& SetData(const std::string& data) {
	    data_ = data;
	    return *this;
	};
	
	void Render(std::ostream& out) const override {
	    out << "<text ";
	    Object::RenderCommon(out);
	    out << " x=\"" << std::to_string(point_.x) << "\"";
	    out << " y=\"" << std::to_string(point_.y) << "\"";
	    out << " dx=\"" << std::to_string(offset_.x) << "\"";
	    out << " dy=\"" << std::to_string(offset_.y) << "\"";
	    out << " font-size=\"" << std::to_string(fontsize_) << "\"";
	    if (fontFamily_) {
		out << " font-family=\"" << fontFamily_.value() << "\""; 
	    }
	    out << ">";
	    out << data_;
	    out << "</text>";
	};
	
	void RenderJSON(std::ostream& out) const override {
	    out << "<text ";
	    Object::RenderCommonJSON(out);
	    out << R"( x=\")" << std::to_string(point_.x) << R"(\")";
	    out << R"( y=\")" << std::to_string(point_.y) << R"(\")";
	    out << R"( dx=\")" << std::to_string(offset_.x) << R"(\")";
	    out << R"( dy=\")" << std::to_string(offset_.y) << R"(\")";
	    out << R"( font-size=\")" << std::to_string(fontsize_) << R"(\")";
	    if (fontFamily_) {
		out << R"( font-family=\")" << fontFamily_.value() << R"(\")"; 
	    }
	    out << ">";
	    out << data_;
	    out << "</text>";
	};
    private:
	Point point_ = {0.0, 0.0};
	Point offset_ = {0.0, 0.0};
	uint32_t fontsize_ = 1;
	std::optional<std::string> fontFamily_;
	std::string data_ = "";
    };
    
    class Document {
    public:
	Document() = default;
	void Render(std::ostream& out) const {
	    out << "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
	    out << "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">";
	    for (auto& p : data_) {
		p->Render(out);
	    }
	    out << "</svg>" << "\n";	
	};
	
	void RenderJSON(std::ostream& out) const {
	    out << R"(<?xml version=\"1.0\" encoding=\"UTF-8\" ?>)";
	    out << R"(<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">)";
	    for (auto& p : data_) {
		p->RenderJSON(out);
	    }
	    out << "</svg>";	
	};
	
	template <typename ObjectType>
	void Add(const ObjectType& obj) {
	    data_.push_back(std::make_unique<ObjectType>(obj));
	};
	
    private:
	std::vector<std::unique_ptr<Object>> data_;
    };
    
} // Svg namespace
