#include <iostream>
#include <vector>

#include "request.h"

using namespace std;

int main() {
    TransportManager manager;
    string input;
    const auto requests = ReadRequestsJSON(cin, input);
    const auto responses = ProcessRequestsJSON(manager, requests, input);
    PrintResponsesJSON(responses);
       
    return 0;
}
