#include <iomanip>
#include <sstream>

#include "json.h"
#include "request.h"

using namespace std;
using namespace Json;

string_view stripLeft(string_view input) {
    while (!input.empty() && input[0] == ' ') {
	input.remove_prefix(1);
    }
    return input;
}

string_view stripRight(string_view input) {
    while (!input.empty() && input[input.size() - 1] == ' ') {
	input.remove_suffix(1);
    }
    return input;
}

string_view strip(string_view input) {
    input = stripLeft(input);
    input = stripRight(input);
    return input;
}

void ReadRouteRequest::ParseFrom(string_view input) {
    auto pos = input.find(' ');
    input.remove_prefix(pos+1);
    name = string(strip(input.substr(0, string::npos)));
}

string ReadRouteRequest::Process(const TransportManager& manager) const {
    stringstream ss;
    manager.DescribeRoute(name, ss);
    return ss.str();
}

void ReadStopRequest::ParseFrom(string_view input) {
    auto pos = input.find(' ');
    input.remove_prefix(pos+1);
    name = string(strip(input.substr(0, string::npos)));
}

string ReadStopRequest::Process(const TransportManager& manager) const {
    stringstream ss;
    manager.DescribeStop(name, ss);
    return ss.str();
}

void CalcPathRequest::ParseFrom(std::string_view input) {
    /* not implemented */
}

string CalcPathRequest::Process(const TransportManager& manager) const {
    /* not implemented */
    return "not implemented";
}

void AddStopRequest::ParseFrom(string_view input) {
    // Remove Request type
    auto pos = input.find(' ');
    input.remove_prefix(pos + 1);

    //Read Stop name
    pos = input.find(':');
    name = string(strip(input.substr(0, pos)));
    input.remove_prefix(pos + 1);

    // Read Latitude
    input = stripLeft(input);
    pos = input.find(',');
    lat = stod(string(input.substr(0, pos)));
    input.remove_prefix(pos + 1);

    // Read Longitude
    input = stripLeft(input);
    pos = input.find(',');
    lon = stod(string(input.substr(0, pos)));
       
    while (pos != string::npos) {
	// Continue parsing the second part of request
	input.remove_prefix(pos + 1);
	input = stripLeft(input);
	pos = input.find('m');
	int dist = stoi(string(input.substr(0, pos)));
	input.remove_prefix(pos + 1);
	
	// Remove to
	input = stripLeft(input);
	pos = input.find(' ');
	input.remove_prefix(pos + 1);
	
	// Read name
	pos = input.find(',');
	string name = string(input.substr(0, pos));
	distances[name] = dist;
	//cout << dist << "m to " << name << endl;
    }
}

void AddStopRequest::Process(TransportManager& manager) const {
    manager.AddStop(name, lat, lon, distances);
}

void AddRouteRequest::ParseStops(std::string_view input,
				 char delimiter) {
    auto pos = input.find(delimiter);
    while (pos != string::npos) {
	stops.push_back(string(strip(input.substr(0, pos))));
	input.remove_prefix(pos + 1);
	pos = input.find(delimiter);
    }
    stops.push_back(string(strip(input.substr(0, pos))));
}

void AddRouteRequest::ParseFrom(string_view input) {
    // Remove Request type
    auto pos = input.find(' ');
    input.remove_prefix(pos + 1);

    // Read Name
    pos = input.find(':');
    name = string(strip(input.substr(0, pos)));
    input.remove_prefix(pos + 1);
    
    input = stripLeft(input);

    // Parse Route
    pos = input.find('>');
    
    if (pos == string::npos) {
	isCircular = false;
	ParseStops(input, '-');
    }
    else {
	isCircular = true;
	ParseStops(input, '>');
    }
}

void AddRouteRequest::Process(TransportManager& manager) const {
    manager.AddRoute(name, stops, isCircular);
}

void SetParametersRequest::ParseFrom(std::string_view input) {
    /* not implemented */
}

void SetParametersRequest::Process(TransportManager& manager) const {
    /* not implemented */
}

RequestHolder Request::Create(Request::Type type) {
    switch (type) {
    case Request::Type::ADD_STOP:
	return make_unique<AddStopRequest>();
    case Request::Type::ADD_ROUTE:
	return make_unique<AddRouteRequest>();
    case Request::Type::READ_STOP:
	return make_unique<ReadStopRequest>();
    case Request::Type::READ_ROUTE:
	return make_unique<ReadRouteRequest>();
    case Request::Type::SET_PARAM:
	return make_unique<SetParametersRequest>();
    case Request::Type::CALC_PATH:
	return make_unique<CalcPathRequest>();
    default:
	return nullptr;
    }
}

optional<Request::Type> GetRequestType(string_view type_str) {
    auto pos = type_str.find(' ');
    string_view type = strip(type_str.substr(0, pos));
    type_str.remove_prefix(pos + 1);
    
    pos = type_str.find(':');
    
    if (type == "Bus") {
	if (pos == string::npos) {
	    return Request::Type::READ_ROUTE;
	}
	else {
	    return Request::Type::ADD_ROUTE;
	}
    }
    else if (type == "Stop") {
	if (pos == string::npos) {
	    return Request::Type::READ_STOP;
	}
	else {
	    return Request::Type::ADD_STOP;
	}
    }
    else {
	return nullopt;
    }
}

RequestHolder ParseRequest(string_view request_str) {
    while (request_str[0] == ' ') {
	request_str.remove_prefix(1);
    }
    
    const auto request_type = GetRequestType(request_str);
    if (!request_type) {
	return nullptr;
    }
    RequestHolder request = Request::Create(*request_type);
    if (request) {
	request->ParseFrom(request_str);
    };
    return request;
}

vector<RequestHolder> ReadRequests(istream& in_stream) {
    size_t request_count;
    string dummy;
    vector<RequestHolder> requests;
    
    while (cin >> request_count) {
	getline(in_stream, dummy);
    	for (size_t i = 0; i < request_count; ++i) {
	    string request_str;
	    getline(in_stream, request_str);
	    if (auto request = ParseRequest(request_str)) {
		requests.push_back(move(request));
	    }
	}
    }
    return requests;
}

vector<string> ProcessRequests(TransportManager& manager,
			       const vector<RequestHolder>& requests) {
    vector<string> responses;
    
    for (const auto& request_holder : requests) {
	if (request_holder->type == Request::Type::READ_ROUTE) {
	    const auto& request = static_cast<const ReadRouteRequest&>(*request_holder);
	    responses.push_back(request.Process(manager));
	}
	else if (request_holder->type == Request::Type::READ_STOP) {
	    const auto& request = static_cast<const ReadStopRequest&>(*request_holder);
	    responses.push_back(request.Process(manager));
	} else {
	    const auto& request = static_cast<const ModifyRequest&>(*request_holder);
	    request.Process(manager);
	}
    }
    return responses;
}

void PrintResponses(const vector<string>& responses,
		    ostream& stream) {
    for (const auto& response : responses) {
	stream << fixed << setprecision(6) << response << endl;
    }
    
}


